var searchData=
[
  ['scene3d_173',['Scene3d',['../classScene3d.html#a921309d97edf6258851a1dc103bf2a12',1,'Scene3d::Scene3d(const Scene3d &amp;scene)'],['../classScene3d.html#a340a150fcdd257b7db57cef03b395bf8',1,'Scene3d::Scene3d(Pave3d cube=Pave3d(), Sphere sphere=Sphere())']]],
  ['setcube_174',['setCube',['../classScene3d.html#aaa40ed27624f1250258eaab9f46ef335',1,'Scene3d']]],
  ['setp1_175',['setP1',['../classTriangle2d.html#a6a3d8778462ef12d08dfe9c77e19e1a5',1,'Triangle2d::setP1()'],['../classTriangle3d.html#a76d313a23c47e661a5135fd72a7ead84',1,'Triangle3d::setP1()']]],
  ['setp2_176',['setP2',['../classTriangle2d.html#acdc82ccf915812f54f1dfb47dc68bae1',1,'Triangle2d::setP2()'],['../classTriangle3d.html#a8eeb9055b3e3c13b9df6716faf2e1115',1,'Triangle3d::setP2()']]],
  ['setp3_177',['setP3',['../classTriangle2d.html#ade27a45929ee04351ebd409e5f4ce48b',1,'Triangle2d::setP3()'],['../classTriangle3d.html#ab21e2d2fd5979b54736be98a8c9cb2c9',1,'Triangle3d::setP3()']]],
  ['setq_178',['setQ',['../classPave3d.html#a39d0c3cb4ba97d1df8e167c17b59d5b8',1,'Pave3d']]],
  ['setquads_179',['setQuads',['../classSphere.html#af60039a877dfdc4f7ab01b01c4880849',1,'Sphere']]],
  ['setsphere_180',['setSphere',['../classScene3d.html#a2b768849b86e00a768d9c994de3bdf81',1,'Scene3d']]],
  ['sett1_181',['setT1',['../classQuad.html#a8a1af35d8928f0fb3eaf83041944f040',1,'Quad']]],
  ['sett2_182',['setT2',['../classQuad.html#aa9f120f8f5d839bad75821126cc91753',1,'Quad']]],
  ['setx_183',['setX',['../classPoint2d.html#a7222f0d19fbf6e8b3bbd0b54666b986e',1,'Point2d::setX()'],['../classVector3.html#abbe67daf4206bc04e357272293573a47',1,'Vector3::setX()']]],
  ['sety_184',['setY',['../classPoint2d.html#ad4ff1915a2fc49be75711b41e353c649',1,'Point2d::setY()'],['../classVector3.html#ac3c9e61dcea9d5a764f193e3d6acd90b',1,'Vector3::setY()']]],
  ['setz_185',['setZ',['../classPoint3d.html#ae12604d2503490275288faf1bae0878e',1,'Point3d::setZ()'],['../classVector3.html#a8c231c89b654632bd02614842bc68ed6',1,'Vector3::setZ()']]],
  ['show_186',['show',['../classSdl.html#a41f61403a956adc385384a25e9be69e4',1,'Sdl']]],
  ['sphere_187',['Sphere',['../classSphere.html#a890a63ff583cb88e7ec4e840b4ef5eb9',1,'Sphere::Sphere()'],['../classSphere.html#ade18846d76540e9a06578e21c0125106',1,'Sphere::Sphere(const std::vector&lt; Quad &gt; &amp;q)'],['../classSphere.html#acc41fe1f50fc2f8d6e71feca602b2925',1,'Sphere::Sphere(const Sphere &amp;s)']]]
];
