var searchData=
[
  ['getcube_135',['getCube',['../classScene3d.html#a6a9b2ebdecc7978d7c69a673f76be3cf',1,'Scene3d']]],
  ['getp1_136',['getP1',['../classTriangle2d.html#a4e8e6564825605d6253d557dcefbc951',1,'Triangle2d::getP1()'],['../classTriangle3d.html#a1d90f38e47c4f579c27bdcec1801ab0f',1,'Triangle3d::getP1()']]],
  ['getp2_137',['getP2',['../classTriangle2d.html#a8ad376edecedb72d9bb3f1e2acc49721',1,'Triangle2d::getP2()'],['../classTriangle3d.html#a21cbf97919392a55e2bbadee32133e42',1,'Triangle3d::getP2()']]],
  ['getp3_138',['getP3',['../classTriangle2d.html#a5dd14dd65bfaf2e05982b6a93b8c2c7a',1,'Triangle2d::getP3()'],['../classTriangle3d.html#a1e8a8fcc77c82f42f1caca2fbdb54a4e',1,'Triangle3d::getP3()']]],
  ['getq_139',['getQ',['../classPave3d.html#a9bdff0058eb14f62d94ec11b7de0f172',1,'Pave3d']]],
  ['getquads_140',['getQuads',['../classSphere.html#a32edbcd18127a4f5524abed162bc9def',1,'Sphere']]],
  ['getsphere_141',['getSphere',['../classScene3d.html#a4416269fa077d2fb51b1b1145ddc9ea4',1,'Scene3d']]],
  ['gett1_142',['getT1',['../classQuad.html#ac0d7c7e7ca191ebdda562d503b388d57',1,'Quad']]],
  ['gett2_143',['getT2',['../classQuad.html#a85af14bc3f90d47f2b66b9995be75e6d',1,'Quad']]],
  ['getx_144',['getX',['../classPoint2d.html#a11b006fc1b63d2abc9b155b2a120c8ee',1,'Point2d::getX()'],['../classVector3.html#a5861be669a169eafccecc09d8e4916b5',1,'Vector3::getX()']]],
  ['gety_145',['getY',['../classPoint2d.html#acd15bdd6114b328c557d32bab888cd3b',1,'Point2d::getY()'],['../classVector3.html#ab24be51ba4d06401f1b8c58578f4713f',1,'Vector3::getY()']]],
  ['getz_146',['getZ',['../classPoint3d.html#a1303d386aa8f230371dfee9e437d23f5',1,'Point3d::getZ()'],['../classVector3.html#a4440fcca47a74910ccc78c9653d03481',1,'Vector3::getZ()']]]
];
