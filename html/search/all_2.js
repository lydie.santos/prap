var searchData=
[
  ['distance_4',['distance',['../classPoint2d.html#ae01b5660587c1a95afe58a20f44c1265',1,'Point2d::distance()'],['../classPoint3d.html#a0c3b3139ef98e34aa30de4a1c3b179aa',1,'Point3d::distance()'],['../classSphere.html#a6a5368f39c3279b2b195eacd61e7e2f3',1,'Sphere::distance()'],['../classTriangle2d.html#a3b33cd1529f949b4ba752bc4af4248f4',1,'Triangle2d::distance()'],['../classTriangle3d.html#a510343f173d40fd236553ba5345a44a2',1,'Triangle3d::distance()']]],
  ['dotproduct_5',['dotProduct',['../classVector3.html#a329665960a99894e6550981a06d1e9f2',1,'Vector3']]],
  ['draw_6',['Draw',['../classSdl.html#ace403e9d2d5d01d3bfb8e84913d73db8',1,'Sdl::Draw(const Triangle2d &amp;t)'],['../classSdl.html#ac4a8558c3696224abe9842dd2f12f031',1,'Sdl::Draw(const Triangle3d &amp;t)'],['../classSdl.html#a74fe415abb6b7241805a703bd1bace3a',1,'Sdl::Draw(const Quad &amp;quad)'],['../classSdl.html#a2d42fb6600ddd279bd80da9ceab481a6',1,'Sdl::Draw(const Pave3d &amp;p)'],['../classSdl.html#ac0ccd1e084b43efa3b46ca486b52d236',1,'Sdl::Draw(const Sphere &amp;s)']]]
];
