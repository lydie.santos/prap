var searchData=
[
  ['triangle2d_188',['Triangle2d',['../classTriangle2d.html#ac3d816ace0a0e7a92fef9f4f7ce56144',1,'Triangle2d::Triangle2d(const Point2d &amp;p1=Point2d(), const Point2d &amp;p2=Point2d(), const Point2d &amp;p3=Point2d())'],['../classTriangle2d.html#a17da0a7ad1328745d14d739f5e465084',1,'Triangle2d::Triangle2d(const Triangle2d &amp;t)']]],
  ['triangle3d_189',['Triangle3d',['../classTriangle3d.html#aff6c1d289b995e26b83eb3f0a58ff7cd',1,'Triangle3d::Triangle3d(const Point3d &amp;p1=Point3d(), const Point3d &amp;p2=Point3d(), const Point3d &amp;p3=Point3d())'],['../classTriangle3d.html#a2a51623d810d9090462f8081656f9b95',1,'Triangle3d::Triangle3d(const Triangle3d &amp;t)'],['../classTriangle3d.html#a6aa546c9d9a748da34e08cd7f6c1b095',1,'Triangle3d::Triangle3d(const Triangle2d &amp;t)']]],
  ['trianglerectangle_190',['triangleRectangle',['../classTriangle2d.html#a103c467fe65341484bc2bce7620562e2',1,'Triangle2d::triangleRectangle()'],['../classTriangle3d.html#a31d8c0b3d4a12716e8b88fc0562c7f26',1,'Triangle3d::triangleRectangle()']]]
];
