/** 
* \file quad3d.cpp
* This file contains the declaration of the quad3d class
*/

#include "../headers/quad3d.h"
#include <cmath>





// ---------------------------------------------- Constructeurs & Destructeur ----------------------------------------------

Quad::Quad(Triangle3d t1, Triangle3d t2)
{
    t1_=t1;
    t2_=t2;
}

Quad::Quad(const Quad &q){
    t1_=q.t1_;
    t2_=q.t2_;
}





// ---------------------------------------------- Getters & Setters ----------------------------------------------

Triangle3d Quad::getT1() const 
{
    return t1_;
}

Triangle3d Quad::getT2() const
{
    return t2_;
}



void Quad::setT1(Triangle3d t)
{
    t1_=t;
}

void Quad::setT2(Triangle3d t)
{
    t2_=t;
}





// ---------------------------------------------- Surcharge d'opérateurs ----------------------------------------------

/**
 * @brief Teste l'égalité entre 2 quadrilatères
 * 
 * @param q le quadrilatère à comparer
 * @return true si les 2 quadrilatères sont égaux, false sinon
*/
bool Quad::operator==(const Quad &q) const
{
    return (t1_ == q.t1_ && t2_ == q.t2_);
}


/**
 * @brief Teste la différence entre 2 quadrilatères
 * 
 * @param q le quadrilatère à comparer
 * @return true si les 2 quadrilatères ne sont pas égaux, false sinon
*/
bool Quad::operator!=(const Quad &q) const
{
    return (t1_ != q.t1_ || t2_ != q.t2_);
}


/**
 * @brief Affecte la valeur d'un quadrilatère
 * 
 * @param q quadrilatère que l'on veut affecter
 * @return rien
*/
Quad & Quad::operator=(const Quad &q)
{
    if (this != &q)
    {
        t1_ = q.t1_;
        t2_ = q.t2_;
    }

    return *this;
    
}





// ---------------------------------------------- Autres méthodes ----------------------------------------------

/**
 * @brief fonction qui teste si le quadrilatère est un rectangle
 *
 * @return true si le quadrilatère est un rectangle, false sinon
 */
bool Quad::isRectangle()
{
    if (t1_.isRectangle() && t2_.isRectangle())
    {
        float p1p2 = t1_.getP1().distance(t1_.getP2());
        float p3p3 = t1_.getP3().distance(t2_.getP3());

        //on récupère les coordonées de I le milieu du segment p1p2 et J le milieu de p3p3
        Point3d i;
        Point3d j;

        i.setX((t1_.getP1().getX()+t1_.getP2().getX())/2);
        i.setY((t1_.getP1().getY()+t1_.getP2().getY())/2);
        i.setZ((t1_.getP1().getZ()+t1_.getP2().getZ())/2);
        j.setX((t1_.getP3().getX()+t2_.getP3().getX())/2);
        j.setY((t1_.getP3().getY()+t2_.getP3().getY())/2);
        j.setZ((t1_.getP3().getZ()+t2_.getP3().getZ())/2);

        if (i==j && p1p2==p3p3)
        {
            return true;
        }
        else 
        {
            return false;
        }
    }
    else 
    {
        return false;
    }
}


/**
 * @brief décale le quadrilatère
 * 
 * @param dx déplacement selon l'axe x
 * @param dy déplacement selon l'axe y
 * @param dz déplacement selon l'axe z
 * @return rien
*/
void Quad::move(float dx, float dy, float dz)
{
    t1_.move(dx, dy, dz);
    t2_.move(dx, dy, dz);
}





// ---------------------------------------------- Méthodes externes ----------------------------------------------

/**
 * @brief Fonction qui créée un rectangle à partir d'un triangle rectangle
 * 
 * @param t triangle rectangle 
 * @return le rectangle créé
*/
Quad quadRectangle(Triangle3d t)
{
    if (!t.isRectangle()) throw "il faut un triangle rectangle";

    float c1 = pow(t.getP2().getX() - t.getP1().getX(), 2) + pow(t.getP2().getY() - t.getP1().getY(), 2) + pow(t.getP2().getZ() - t.getP1().getZ(), 2); 
    float c2 = pow(t.getP3().getX() - t.getP2().getX(), 2) + pow(t.getP3().getY() - t.getP2().getY(), 2) + pow(t.getP3().getZ() - t.getP2().getZ(), 2); 
    float c3 = pow(t.getP1().getX() - t.getP3().getX(), 2) + pow(t.getP1().getY() - t.getP3().getY(), 2) + pow(t.getP1().getZ() - t.getP3().getZ(), 2); 

    float epsilon = 1e-3;

    // soient p1p2 l'hypothénuse et p3 le point qui forme l'angle droit du triangle
    Point3d p1;
    Point3d p2;
    Point3d p3;

    if (abs(c1 - (c2 + c3)) < epsilon )
    {
        p1=t.getP1();
        p2=t.getP2();
        p3=t.getP3();
    }
    else if (abs(c2 - (c3 + c1)) < epsilon )
    {
        p1=t.getP2();
        p2=t.getP3();
        p3=t.getP1();
    }
    else if (abs(c3 - (c2 + c1)) < epsilon )
    {
        p1=t.getP1();
        p2=t.getP3();
        p3=t.getP2();
    }

    float Dx = p1.getX() + p2.getX() - p3.getX();
    float Dy = p1.getY() + p2.getY() - p3.getY();
    float Dz = p1.getZ() + p2.getZ() - p3.getZ();
    Point3d D(Dx, Dy, Dz);


    Triangle3d other = Triangle3d(p1,p2,D);
    return Quad(t, other);
}