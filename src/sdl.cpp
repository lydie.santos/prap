/** 
* \file sdl.cpp
*/

#include "../headers/sdl.h"

#include <algorithm>

       

/**
 * Initialisation des variables statiques
*/
bool Sdl::is_running = true;
std::list<pevent> Sdl::events;
void* Sdl::win_;
void* Sdl::ren_;


/**
 * @brief récupération de renderer
 * @return le renderer
*/
void* Sdl::getRen() 
{
    return Sdl::ren_;
}


/**
 * @brief initialisation de sdl
 * 
 * @return rien
*/
void Sdl::init()
{
    //initialisation de SDL
    if(SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        //SDL n'est pas initialisé
        //WhatException::what(); //throw à catch dans le main
    }
}


/**
 * @brief termine la sdl
 * 
 * @return rien
*/
void Sdl::quit()
{
    SDL_Quit();
}


/**
 * @brief Initialise la fenêtre
 * 
 * @param titre le titre de la fene+être
 * @param pos la position de la fenêtre
 * @param size la taille de la fenêtre
*/
void Sdl::init_window(std::string &titre, position &pos, size &s)
{
    //Création de la fenêtre
    win_ = SDL_CreateWindow(titre.c_str(), pos[0], pos[1], s[0], s[1], SDL_WINDOW_OPENGL);
    
    if (!win_)
    {
        throw("Problème lors de la création de la fenêtre");
    }

    //Création du renderer associé à la denêtre
    ren_ = SDL_CreateRenderer(reinterpret_cast<SDL_Window*>(win_), -1, 0);

    if (!ren_)
    {
        throw("Problème lors de la création du renderer");
    }

    //On met le fond en noir
    SDL_SetRenderDrawColor(reinterpret_cast<SDL_Renderer*>(ren_), 0, 0, 0, SDL_ALPHA_OPAQUE);

    //On efface le buffer
    SDL_RenderClear(reinterpret_cast<SDL_Renderer*>(ren_));
}


/**
 * @brief Destruction de la denêtre
*/
void Sdl::destroy_window()
{
    SDL_RenderClear(reinterpret_cast<SDL_Renderer*>(ren_));
    SDL_DestroyRenderer(reinterpret_cast<SDL_Renderer*>(ren_));
    SDL_DestroyWindow(reinterpret_cast<SDL_Window*>(win_));
    SDL_Quit();
}


/**
 * @brief Affiche la fenêtre
 * 
 * @return rien
*/
void Sdl::show()
{
    SDL_ShowWindow(reinterpret_cast<SDL_Window*>(win_));
}

/**
 * @brief Cache la fenêtre
 * 
 * @return rien
*/
void Sdl::hide()
{
    SDL_HideWindow(reinterpret_cast<SDL_Window*>(win_));
}


/**
 * @brief Actualise la fenêtre
 * 
 * @return rien
*/
void Sdl::present()
{
    SDL_RenderPresent(reinterpret_cast<SDL_Renderer*>(ren_));
}


/**
 * @brief Fonction qui projete un Point3d sur le plan d'une scène
 * 
 * @param s la scène où l'on projète le point
 * @param p le point à projeter sur la scène
 * @return La projection du point dans la scène
*/
Point2d Sdl::Projection(const Point3d& p) {
    double distance = 5.0;
    double scale = 100.0;
    // Projection orthographique sur le plan Z=0
    //double scale = distance / (distance - p.getZ());
    double projectedX = scale * p.getX();
    double projectedY = scale * p.getY();

    return Point2d(projectedX, projectedY);
    //TODO projection à revoir 

}
        
        
/**
 * @brief Fonction qui trace un triangle 2d
 * 
 * @param win fenêtre où l'on trace le triangle
 * @param t triangle à tracer
*/
void Sdl::Draw(const Triangle2d &t)
{
    //Couleur des arrêtes : blanc
    SDL_SetRenderDrawColor(reinterpret_cast<SDL_Renderer*>(ren_), 255, 255, 255, SDL_ALPHA_OPAQUE);

    Point2d p1 = t.getP1();
    Point2d p2 = t.getP2();
    Point2d p3 = t.getP3();

    //On trace une ligne entre p1 et p2
    SDL_RenderDrawLine(reinterpret_cast<SDL_Renderer*>(ren_), p1.getX(), p1.getY(), p2.getX(), p2.getY());
    //On trace une ligne entre p2 et p3
    SDL_RenderDrawLine(reinterpret_cast<SDL_Renderer*>(ren_), p2.getX(), p2.getY(), p3.getX(), p3.getY());
    //On trace une ligne entre p3 et p1
    SDL_RenderDrawLine(reinterpret_cast<SDL_Renderer*>(ren_), p3.getX(), p3.getY(), p1.getX(), p1.getY());

    SDL_SetRenderDrawColor(reinterpret_cast<SDL_Renderer*>(ren_), 255, 255, 255, SDL_ALPHA_OPAQUE);

    // Obtenez les coordonnées minimales et maximales du triangle
    float minX = std::min({p1.getX(), p2.getX(), p3.getX()});
    float minY = std::min({p1.getY(), p2.getY(), p3.getY()});
    float maxX = std::max({p1.getX(), p2.getX(), p3.getX()});
    float maxY = std::max({p1.getY(), p2.getY(), p3.getY()});


    for (int y = minY; y <= maxY; ++y) {

        float x1 = t.interpolate(p1.getY(), p2.getY(), p3.getY(), p1.getX(), p2.getX(), p3.getX(), y);
        float x2 = t.interpolate(p1.getY(), p2.getY(), p3.getY(), p1.getX(), p2.getX(), p3.getX(), y + 1);

        for (int x = std::min(static_cast<int>(x1), static_cast<int>(x2)); x <= std::max(static_cast<int>(x1), static_cast<int>(x2)); ++x) {
            SDL_RenderDrawPoint(reinterpret_cast<SDL_Renderer*>(ren_), x, y);
        }
    }

}





/**
 * @brief Fonction qui trace un triangle 3d
 * 
 * @param win fenêtre où l'on trace le triangle
 * @param t triangle à tracer
*/
void Sdl::Draw(const Triangle3d &t)
{
    // Couleur des arrêtes : blanc
    SDL_SetRenderDrawColor(reinterpret_cast<SDL_Renderer*>(ren_), 255, 255, 255, SDL_ALPHA_OPAQUE);

    Point3d p1 = t.getP1();
    Point3d p2 = t.getP2();
    Point3d p3 = t.getP3();

    // Projection des points 3D sur le plan 2D
    Point2d P1_proj = Projection(p1);
    Point2d P2_proj = Projection(p2);
    Point2d P3_proj = Projection(p3);

    // On trace une ligne entre p1 et p2
    SDL_RenderDrawLine(reinterpret_cast<SDL_Renderer*>(ren_), P1_proj.getX(), P1_proj.getY(), P2_proj.getX(), P2_proj.getY());
    // On trace une ligne entre p2 et p3
    SDL_RenderDrawLine(reinterpret_cast<SDL_Renderer*>(ren_), P2_proj.getX(), P2_proj.getY(), P3_proj.getX(), P3_proj.getY());
    // On trace une ligne entre p3 et p1
    SDL_RenderDrawLine(reinterpret_cast<SDL_Renderer*>(ren_), P3_proj.getX(), P3_proj.getY(), P1_proj.getX(), P1_proj.getY());
}


/**
 * @brief Fonction qui trace un quadrilatère
 * 
 * @param win fenêtre où l'on trace le quadrilatère
 * @param q quadrilatère à tracer
*/
void Sdl::Draw(const Quad &q)
{
    Triangle3d t1 = q.getT1();
    Triangle3d t2 = q.getT2();

    // TODO : voir si on draw la ligne entre les 2 triangles ou pas
    Sdl::Draw(t1);
    Sdl::Draw(t2);
}


/**
 * @brief Fonction qui trace un pave
 * 
 * @param win fenêtre où l'on trace le pave
 * @param p pave à tracer
*/
void Sdl::Draw(const Pave3d &p)
{
    //Quad* q = p.getQ();

    // TODO : voir si on draw toutes les lignes
    for(int i = 0; i<6; i++) //Un pavé est composé de 6 quadrilatères
    {
        Sdl::Draw(p[i]);
    }
}


/**
 * @brief Fonction qui trace une sphère
 * 
 * @param win fenêtre où l'on trace la sphère
 * @param s sphère à tracer
*/
void Sdl::Draw(const Sphere &s)
{
    // TODO : voir comment on gère le pointeur (est-ce qu'on connaît sa taille ou pas ect)
}


/**
 * @brief Lance la boucle infinie permettant de faire bouger le cube
 * 
 * @return rien
*/
void Sdl::run()
{
    while (is_running)
    {
        SDL_Event event;
        if(SDL_PollEvent(&event))
        {
            switch(event.type)
            {
                //Si on force l'arrêt
                case (SDL_QUIT):
                {
                    exit_run();
                    break;
                }
                
                //Si une touche est pressée
                case (SDL_KEYDOWN):
                {
                    //On récupère la touche
                    int key = event.key.keysym.sym; 

                    //Pour chaque éléments dans la liste des événements possible, on appelle la fonction callback
                    for (auto e : events)
                    {
                        e->callback(key);
                    }

                    break;
                }
            }
            
        }
    }
}


/**
 * @brief Fonction permettant de sortir de la boucle infinie
 * 
 * @return rien
*/
void Sdl::exit_run()
{
    is_running = false;
}


void Sdl::add_event(const pevent & e)
{
    events.push_back(e);
}