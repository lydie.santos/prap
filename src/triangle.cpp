/** 
* \file triangle.cpp
* This file contains the declaration of the triangle2d and triangle3d class
*/


#include "../headers/triangle.h"
#include <math.h>
#include <iostream>
#include <algorithm>


#include <cstdlib>
#include <ctime>




/**
 * -------------------------------------------------------------------------------------------------------------
 * ------------------------------------------------ Triangle2d ------------------------------------------------
 * -------------------------------------------------------------------------------------------------------------
*/

// ---------------------------------------------- Constructeurs ----------------------------------------------

Triangle2d::Triangle2d(const Point2d &p1, const Point2d &p2, const Point2d &p3)
{
    p1_ = p1;
    p2_ = p2;
    p3_ = p3;
}


Triangle2d::Triangle2d(const Triangle2d &t)
{
    p1_ = t.p1_;
    p2_ = t.p2_;
    p3_ = t.p3_;
}



// ---------------------------------------------- Getters & Setters ----------------------------------------------

Point2d Triangle2d::getP1() const
{
    return p1_;
}


Point2d Triangle2d::getP2() const
{
    return p2_;
}


Point2d Triangle2d::getP3() const
{
    return p3_;
}

SDL_Color Triangle2d::getColor() const
{
    return color_;
}


void Triangle2d::setP1(Point2d p)
{
    p1_ = p;
}


void Triangle2d::setP2(Point2d p)
{
    p2_ = p;
}


void Triangle2d::setP3(Point2d p)
{
    p3_ = p;
}


void Triangle2d::setColor(SDL_Color color)
{
    color_ = color;
}




// ---------------------------------------------- Surcharge d'opérateurs ----------------------------------------------

bool Triangle2d::operator==(const Triangle2d &t) const
{
    if ((p1_ == t.p1_) && (p2_ == t.p2_) && (p3_ == t.p3_))
    {
        return true;
    }
    
    return false;
}


bool Triangle2d::operator!=(const Triangle2d &t) const
{
    return !(*this == t);
}


Triangle2d & Triangle2d::operator=(const Triangle2d &t)
{
    if (this != &t)
    {
        p1_ = t.p1_;
        p2_ = t.p2_;
        p3_ = t.p3_;
    }
    return *this;
}



// ---------------------------------------------- Autres méthodes ----------------------------------------------

/**
 * @brief Fonction qui créée un triangle rectangle. Elle prend en argument 2 points et place le troisième de façon 
 * à former un triangle rectangle
 * 
 * @param p1 Premier point du triangle
 * @param p2 Deuxième point du triangle
 * @return le triangle rectangle créé
 */
Triangle2d Triangle2d::triangleRectangle(const Point2d &p1, const Point2d &p2)
{
    Point2d p3; //dernier point du triangle

    //on récupère les coordonées de I le milieu du segment p1-p2

    Point2d i;

    i.setX((p1.getX()+p2.getX())/2);
    i.setY((p1.getY()+p2.getY())/2);

    //on créée un point aléatoirement sur le cercle de centre I et de diamètre p1-p2

    float d = p1.distance(p2);
    float r = d/2;

    //on prend un x aléatoire entre le x de p1 et le x de p2

    std::srand(static_cast<unsigned>(std::time(0)));

    float x =static_cast<float>(std::rand()) / RAND_MAX * (p2.getX() - p1.getX()) + p1.getX();

    p3.setX(x);

    // on choisit y grâce à (x-xi)^2+(y-yi)^2=r^2
    p3.setY(pow(r*r-pow(x-i.getX(),2),0.5)+i.getY());

    return Triangle2d(p1, p2, p3);
}


/**
 * @brief fonction qui teste si le triangle est rectangle
 *
 * @return true si le triangle est rectangle, false sinon
 */
bool Triangle2d::isRectangle()
{
    //Pythagore : c1 = c2 + c3 avec c1, c2 et c3 les côtés du triangles au carré
    float c1 = pow(p2_.getX() - p1_.getX(), 2) + pow(p2_.getY() - p1_.getY(), 2); //p1p2^2 = (x2 - x2)^2 + (y2 - y1)^2
    float c2 = pow(p3_.getX() - p2_.getX(), 2) + pow(p3_.getY() - p2_.getY(), 2); //p2p3
    float c3 = pow(p1_.getX() - p3_.getX(), 2) + pow(p1_.getY() - p3_.getY(), 2); //p3p1

    // il faut rajouter une condition sur les erreurs d'arrondi sinon les tests ne passent pas 
    float epsilon = 1e-3;
    if (abs(c1 - (c2 + c3)) < epsilon || abs(c2 - (c3 + c1)) < epsilon || abs(c3 - (c2 + c1)) < epsilon) return true;

    return false;
}


/**
 * @brief décale le triangle
 * 
 * @param dx déplacement selon l'axe x
 * @param dy déplacement selon l'axe y
 * @return rien
*/
void Triangle2d::move(float dx, float dy)
{
    float x1 = p1_.getX();
    float y1 = p1_.getY();
    p1_.setX(x1 + dx);
    p1_.setY(y1 + dy);
    
    float x2 = p2_.getX();
    float y2 = p2_.getY();
    p2_.setX(x2 + dx);
    p2_.setY(y2 + dy);

    float x3 = p3_.getX();
    float y3 = p3_.getY();
    p3_.setX(x3 + dx);
    p3_.setY(y3 + dy);
}


/**
 * @brief calcule la distance entre 2 triangles (distance entre les barycentres)
 * 
 * @param t le triangle par rapport auquel on calcule la distance
 * @return la distance entre les 2 triangles
*/
float Triangle2d::distance(const Triangle2d &t)
{
    //On commence par calculer les barycentres des 2 triangles
    int this_x = (p1_.getX() + p2_.getX() + p3_.getX()) / 3;
    int this_y = (p1_.getY() + p2_.getY() + p3_.getY()) / 3;
    Point2d this_bary = Point2d(this_x, this_y);

    int t_x = (t.getP1().getX() + t.getP3().getX() + t.getP3().getX()) / 3;
    int t_y = (t.getP1().getY() + t.getP2().getY() + t.getP3().getY()) / 3;
    Point2d t_bary = Point2d(t_x, t_y);

    //On calcule la distance entre les 2 centres de masses
    float dist = this_bary.distance(t_bary);

    return dist;
}


/**
 * @brief Interpolation linéaire
 * 
 * @return La valeur interpolée
 */
float Triangle2d::interpolate(float y1, float y2, float y3, float x1, float x2, float x3, float y) const {
    float denom = (y2 - y3) * (x1 - x3) + (x3 - x2) * (y1 - y3);
    float alpha = ((y2 - y3) * (x1 - x3) + (x3 - x2) * (y1 - y3)) / denom;
    float beta = ((y3 - y1) * (x1 - x3) + (x1 - x2) * (y1 - y3)) / denom;
    float gamma = 1 - alpha - beta;
std::cout << "alpha: " << alpha << ", beta: " << beta << ", gamma: " << gamma << std::endl;
std::cout << "x1: " << x1 << ", x2: " << x2 << ", x3: " << x3 << std::endl;

    return alpha * p1_.getX() + beta * p2_.getX() + gamma * p3_.getX();
}



/**
 * @brief remplissage d'un triangle 
 * 
 * @param c la couleur avec laquelle on doit colorier le triangle
 * @return rien
*/
void Triangle2d::remplissage(SDL_Renderer* renderer, const SDL_Color& c) const {
    SDL_SetRenderDrawColor(renderer, c.r, c.g, c.b, c.a);

    // Obtenez les coordonnées minimales et maximales du triangle
    float minX = std::min({p1_.getX(), p2_.getX(), p3_.getX()});
    float minY = std::min({p1_.getY(), p2_.getY(), p3_.getY()});
    float maxX = std::max({p1_.getX(), p2_.getX(), p3_.getX()});
    float maxY = std::max({p1_.getY(), p2_.getY(), p3_.getY()});

    // Dessinez des rectangles horizontaux pour remplir le triangle
    for (int y = minY; y <= maxY; ++y) {
        float x1 = interpolate(p1_.getY(), p2_.getY(), p3_.getY(), p1_.getX(), p2_.getX(), p3_.getX(), y);
        float x2 = interpolate(p1_.getY(), p2_.getY(), p3_.getY(), p1_.getX(), p2_.getX(), p3_.getX(), y + 1);

        // Dessinez le rectangle horizontal
        SDL_Rect rect = {static_cast<int>(x1), y, static_cast<int>(x2 - x1), 1};
        SDL_RenderFillRect(renderer, &rect);
    }
}



/**
 * -------------------------------------------------------------------------------------------------------------
 * ------------------------------------------------ Triangle3d ------------------------------------------------
 * -------------------------------------------------------------------------------------------------------------
*/

// ---------------------------------------------- Constructeurs ----------------------------------------------

Triangle3d::Triangle3d(const Point3d &p1, const Point3d &p2, const Point3d &p3)
{
    p1_ = p1;
    p2_ = p2;
    p3_ = p3;
}


Triangle3d::Triangle3d(const Triangle3d &t)
{
    p1_ = t.p1_;
    p2_ = t.p2_;
    p3_ = t.p3_;
}

Triangle3d::Triangle3d(const Triangle2d &t)
{
    p1_ = Point3d(t.getP1());
    p2_ = Point3d(t.getP2());
    p3_ = Point3d(t.getP3());
}



// ---------------------------------------------- Getters & Setters ----------------------------------------------

Point3d Triangle3d::getP1() const
{
    return p1_;
}


Point3d Triangle3d::getP2() const
{
    return p2_;
}


Point3d Triangle3d::getP3() const 
{
    return p3_;
}


SDL_Color Triangle3d::getColor() const
{
    return color_;
}


void Triangle3d::setP1(Point3d p)
{
    p1_ = p;
}


void Triangle3d::setP2(Point3d p)
{
    p2_ = p;
}


void Triangle3d::setP3(Point3d p)
{
    p3_ = p;
}


void Triangle3d::setColor(SDL_Color color)
{
    color_ = color;
}


// ---------------------------------------------- Surcharge d'opérateurs ----------------------------------------------

bool Triangle3d::operator==(const Triangle3d &t) const
{
    return (p1_ == t.p1_) && (p2_ == t.p2_) && (p3_ == t.p3_);
}


bool Triangle3d::operator!=(const Triangle3d &t) const
{
    return !(*this == t);
}


Triangle3d & Triangle3d::operator=(const Triangle3d &t)
{
    if (this != &t)
    {
        p1_ = t.p1_;
        p2_ = t.p2_;
        p3_ = t.p3_;
    }
    return *this;
}



// ---------------------------------------------- Autres méthodes ----------------------------------------------

/**
 * @brief Fonction qui créée un triangle rectangle. Elle prend en argument 2 points et place le troisième de façon 
 * à former un triangle rectangle
 * 
 * @param p1 Premier point du triangle
 * @param p2 Deuxième point du triangle
 * @return le triangle créé
*/
Triangle3d Triangle3d::triangleRectangle(const Point3d &p1, const Point3d &p2) // Juste 2 points car le troisième sera créé pour qu'il soit rectangle
{

    Point3d p3;

    //on récupère les coordonées de I le milieu du segment p1-p2

    Point3d i;

    i.setX((p1.getX()+p2.getX())/2);
    i.setY((p1.getY()+p2.getY())/2);
    i.setZ((p1.getZ()+p2.getZ())/2);

    //on créée un point aléatoirement sur la sphère de centre I et de diamètre p1-p2

    float d = p1.distance(p2);
    float r = d/2;



    float theta =static_cast<float>(std::rand()) / RAND_MAX * 2 * M_PI;
    float phi =static_cast<float>(std::rand()) / RAND_MAX * M_PI;


    float x=r*sin(phi)*cos(theta);
    float y=r*sin(phi)*sin(theta);
    float z=r*cos(phi);


    p3.setX(x+i.getX());
    p3.setY(y+i.getY());
    p3.setZ(z+i.getZ());

    return Triangle3d(p1, p2, p3);
}


/**
 * @brief fonction qui teste si le triangle est rectangle
 *
 * @return true si le triangle est rectangle, false sinon
 */
bool Triangle3d::isRectangle()
{
    float d1_2 = pow(p1_.distance(p2_),2);
    float d2_2 = pow(p1_.distance(p3_),2);
    float d3_2 = pow(p2_.distance(p3_),2);

    // il faut rajouter une condition sur les erreurs d'arrondi sinon les tests ne passent pas 
    float epsilon = 1e-3;
    if (abs(d1_2 - (d2_2+d3_2)) < epsilon || abs(d2_2 - (d1_2+d3_2)) < epsilon || abs(d3_2 - (d1_2+d2_2)) < epsilon) return true;

    return false; 
}


/**
 * @brief décale le triangle
 * 
 * @param dx déplacement selon l'axe x
 * @param dy déplacement selon l'axe y
 * @param dz déplacement selon l'axe z
 * @return rien
*/
void Triangle3d::move(float dx, float dy, float dz)
{
    float x1 = p1_.getX();
    float y1 = p1_.getY();
    float z1 = p1_.getZ();
    p1_.setX(x1 + dx);
    p1_.setY(y1 + dy);
    p1_.setZ(z1 + dz);
    
    float x2 = p2_.getX();
    float y2 = p2_.getY();
    float z2 = p2_.getZ();
    p2_.setX(x2 + dx);
    p2_.setY(y2 + dy);
    p2_.setZ(z2 + dz);

    float x3 = p3_.getX();
    float y3 = p3_.getY();
    float z3 = p3_.getZ();
    p3_.setX(x3 + dx);
    p3_.setY(y3 + dy);
    p3_.setZ(z3 + dz);
}


/**
 * @brief calcule la distance entre 2 triangles (distance entre les barycentres)
 * 
 * @param t le triangle par rapport auquel on calcule la distance
 * @return la distance entre les 2 triangles
*/
float Triangle3d::distance(const Triangle3d &t)
{
    //On commence par calculer les barycentres des 2 triangles
    int this_x = (p1_.getX() + p2_.getX() + p3_.getX()) / 3;
    int this_y = (p1_.getY() + p2_.getY() + p3_.getY()) / 3;
    int this_z = (p1_.getZ() + p2_.getZ() + p3_.getZ()) / 3;
    Point3d this_bary = Point3d(this_x, this_y, this_z);

    int t_x = (t.getP1().getX() + t.getP3().getX() + t.getP3().getX()) / 3;
    int t_y = (t.getP1().getY() + t.getP2().getY() + t.getP3().getY()) / 3;
    int t_z = (t.getP1().getZ() + t.getP2().getZ() + t.getP3().getZ()) / 3;
    Point3d t_bary = Point3d(t_x, t_y, t_z);

    //On calcule la distance entre les 2 centres de masses
    float dist = this_bary.distance(t_bary);

    return dist;
}


/**
 * @brief Interpolation linéaire
 * 
 * @return La valeur interpolée
 */
float Triangle3d::interpolate(float y1, float y2, float y3, float x1, float x2, float x3, float z1, float z2, float z3, float y) const {
    float denom = (y2 - y3) * (x1 - x3) + (x3 - x2) * (y1 - y3);
    float alpha = ((y2 - y3) * (x1 - x3) + (x3 - x2) * (y1 - y3)) / denom;
    float beta = ((y3 - y1) * (x1 - x3) + (x1 - x2) * (y1 - y3)) / denom;
    float gamma = 1 - alpha - beta;

    float z = alpha * z1 + beta * z2 + gamma * z3;
    return z;
}


/**
 * @brief remplissage d'un triangle 
 * 
 * @param c la couleur avec laquelle on doit colorier le triangle
 * @return rien
*/
void Triangle3d::remplissage(SDL_Renderer* renderer, const SDL_Color& c) const {
    SDL_SetRenderDrawColor(renderer, c.r, c.g, c.b, c.a);

    // Obtenez les coordonnées minimales et maximales du triangle
    float minX = std::min({p1_.getX(), p2_.getX(), p3_.getX()});
    float minY = std::min({p1_.getY(), p2_.getY(), p3_.getY()});
    float minZ = std::min({p1_.getZ(), p2_.getZ(), p3_.getZ()});
    float maxX = std::max({p1_.getX(), p2_.getX(), p3_.getX()});
    float maxY = std::max({p1_.getY(), p2_.getY(), p3_.getY()});
    float maxZ = std::max({p1_.getZ(), p2_.getZ(), p3_.getZ()});

    // Dessinez des rectangles horizontaux pour remplir le triangle
    for (int y = minY; y <= maxY; ++y) {
        for (int z = minZ; z <= maxZ; ++z) {
            float x1 = interpolate(p1_.getY(), p2_.getY(), p3_.getY(), p1_.getX(), p2_.getX(), p3_.getX(), p1_.getZ(), p2_.getZ(), p3_.getZ(), y);
            float x2 = interpolate(p1_.getY(), p2_.getY(), p3_.getY(), p1_.getX(), p2_.getX(), p3_.getX(), p1_.getZ(), p2_.getZ(), p3_.getZ(), y + 1);

            // Dessinez le rectangle horizontal
            SDL_Rect rect = {static_cast<int>(x1), y, static_cast<int>(x2 - x1), 1};
            SDL_RenderFillRect(renderer, &rect);
        }
    }
}