/** 
* \file scene3d.cpp
* This file contains the declaration of the scene3d class
*/

#include "../headers/scene3d.h"





// ----------------------------------------- Constructeurs & Destructeur -----------------------------------------

/**
 * @brief Constructeur par défaut et valué
 * 
 * @param cube le cube composant la scène
 * @param sphere la sphère composant la scène
*/
Scene3d::Scene3d(Pave3d cube, Sphere sphere)
{
    cube_ = cube;
    sphere_ = sphere;
}


/**
 * @brief Constructeur de copie
 * 
 * @param scene la scène à copier
*/
Scene3d::Scene3d(const Scene3d &scene)
{
    cube_ = scene.cube_;
    sphere_ = scene.sphere_;
}


// ---------------------------------------------- Getters & Setters ----------------------------------------------

/**
 * @brief Getter du cube composant la scène
 * 
 * @return le cube composant la scène
*/
Pave3d Scene3d::getCube()
{
    return cube_;
}


/**
 * @brief Getter de la sphère composant la scène
 * 
 * @return la sphère composant la scène
*/
Sphere Scene3d::getSphere()
{
    return sphere_;
}


/**
 * @brief Setter du cube composant la scène
 * 
 * @param cube le cube à instancier comme cube de la scène
 * @return rien
*/
void Scene3d::setCube(Pave3d cube)
{
    cube_ = cube;
}


/**
 * @brief Setter de la sphère composant la scène
 * 
 * @param sphere la sphère à instancier comme sphère de la scène
 * @return rien
*/
void Scene3d::setSphere(Sphere sphere)
{
    sphere_ = sphere;
}



// ---------------------------------------------- Autres méthodes ----------------------------------------------