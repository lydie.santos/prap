/** 
* \file sphere3d.cpp
* This file contains the declaration of the sphere3d class
*/

#include "../headers/sphere3d.h"


//TODO vérifier les constructeurs


// ---------------------------------------------- Constructeurs & Destructeur ----------------------------------------------
        
/**
 * @brief Constructeur par défaut
 */
Sphere::Sphere()
{
    unsigned long int taille=500;
    quads_.resize(taille, Quad());
}


/**
 * @brief Constructeur valué
 * 
 * @param q ensemble des quadrilatères formant la sphère
*/
Sphere::Sphere(const std::vector<Quad> &q)
{
    quads_=q;
}


/**
 * @brief Constructeur de copie
 * 
 * @param s la sphere à copier
*/
Sphere::Sphere (const Sphere & s)
{
    quads_=s.quads_;
}





// ---------------------------------------------- Getters & Setters ----------------------------------------------

/**
 * @brief Getter de quads
 * 
 * @return un pointeur vers l'ensemble de quadrilatère qui compose la sphère
*/
std::vector<Quad> Sphere::getQuads() const
{
    return quads_;
}

/**
 * @brief Setter de quads
 * 
 * @param q l'ensemble de quadrilatère à donner à quads
 * @return rien
*/
void Sphere::setQuads(std::vector<Quad> q)
{
    quads_=q;
}





// ---------------------------------------------- Surcharge d'opérateurs ----------------------------------------------

/**
 * @brief Teste l'égalité entre 2 spheres
 * 
 * @param s la sphère à comparer
 * @return true si les 2 sphères sont égales, false sinon
*/
bool Sphere::operator==(const Sphere &s) const
{
    if (quads_.size()!=s.quads_.size())
    {
        return false;
    }
    else
    {
        for (long unsigned int i = 0; i < quads_.size(); ++i)
        {
            if (quads_[i].operator!=(s.quads_[i]))
            {
                return false;
            }
        }
        return true;
    }
}





/**
 * @brief affecte à une sphère une autre sphère
 * 
 * @param s la sphère à affecter
 * @return la nouvelle sphère
*/
Sphere & Sphere::operator=(const Sphere &s)
{
    if (this != &s)
    {
        for (long unsigned int i=0; i<s.quads_.size(); i++)
        {
            quads_=s.quads_;
        }
    }
    return *this;
}


// ---------------------------------------------- Autres méthodes ----------------------------------------------

/**
 * @brief décale la sphère
 * 
 * @param dx déplacement selon l'axe x
 * @param dy déplacement selon l'axe y
 * @param dz déplacement selon l'axe z
 * @return rien
*/
void Sphere::move(float dx, float dy, float dz)
{
    for (long unsigned int i = 0; i < quads_.size(); ++i)
    {
        quads_[i].move(dx, dy, dz);
    }
}