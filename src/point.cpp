/** 
* \file point.cpp
* This file contains the declaration of the point2d and point3d class
*/

#include "../headers/point.h"
#include <math.h>





/**
 * -------------------------------------------------------------------------------------------------------------
 * -------------------------------------------------- Point 2d --------------------------------------------------
 * -------------------------------------------------------------------------------------------------------------
*/

// ---------------------------------------------- Constructeurs & Destructeur ----------------------------------------------

Point2d::Point2d(float x, float y)
{
    x_ = x;
    y_ = y;
}


Point2d::Point2d(const Point2d &p)
{
    x_ = p.x_;
    y_ = p.y_;
}



// ---------------------------------------------- Getters & Setters ----------------------------------------------

float Point2d::getX() const
{
    return x_;
}


float Point2d::getY() const
{
    return y_;
}


void Point2d::setX(float x)
{
    x_ = x;
}


void Point2d::setY(float y)
{
    y_ = y;
}



// ---------------------------------------------- Surcharge d'opérateurs ----------------------------------------------

bool Point2d::operator==(const Point2d &p) const
{
    if ((x_ == p.x_) && (y_ == p.y_))
    {
        return true;
    }
    
    return false;
}


bool Point2d::operator!=(const Point2d &p) const
{
    return !(*this == p);
}


Point2d & Point2d::operator=(const Point2d &p)
{
    if (this != &p)
    {
        x_ = p.x_;
        y_ = p.y_;
    }
    
    return *this;
}


Point2d & Point2d::operator*=(const float &f)
{
    x_ *= f;
    y_ *= f;

    return *this;
}



// ---------------------------------------------- Autres méthodes ----------------------------------------------

/**
 * @brief décale le point
 * 
 * @param dx déplacement selon l'axe x
 * @param dy déplacement selon l'axe y
 * @return rien
*/
void Point2d::move(float dx, float dy)
{
    x_ += dx;
    y_ += dy;
}


/**
 * @brief calcule la distance entre 2 points
 * 
 * @param p le point par rapport auquel on calcule la distance
 * @return la distance entre les 2 points
*/
float Point2d::distance(const Point2d &p) const 
{
    float val = pow((p.x_ - x_), 2) + pow((p.y_ - y_), 2);

    return sqrt(val);
}





/**
 * -------------------------------------------------------------------------------------------------------------
 * -------------------------------------------------- Point 3d --------------------------------------------------
 * -------------------------------------------------------------------------------------------------------------
*/

// ---------------------------------------------- Constructeurs & Destructeur ----------------------------------------------

Point3d::Point3d(float x, float y, float z) : Point2d(x, y)
{
    z_ = z;
}


Point3d::Point3d(const Point2d &p) : Point2d(p) 
{
    z_ = 0;
}

Point3d::Point3d(const Point3d &p): Point2d(p) 
{
    z_ = p.z_;
}



// ---------------------------------------------- Getters & Setters ----------------------------------------------

float Point3d::getZ() const
{
    return z_;
}


void Point3d::setZ(float z)
{
    z_=z;
}



// ---------------------------------------------- Surcharge d'opérateurs ----------------------------------------------

bool Point3d::operator==(const Point3d &p) const
{
    return (x_ == p.x_) && (y_ == p.y_) && (z_ == p.z_);
}


bool Point3d::operator!=(const Point3d &p) const
{
    return !(*this == p);
}


Point3d & Point3d::operator=(const Point3d &p){
    if (this != &p)
    {
        x_ = p.x_;
        y_ = p.y_;
        z_ = p.z_;
    }
    return *this;
}


Point3d & Point3d::operator*=(const float &f)
{
    x_ *= f;
    y_ *= f;
    z_ *= f;

    return *this;
}



// ---------------------------------------------- Autres méthodes ----------------------------------------------

/**
 * @brief décale le point
 * 
 * @param dx déplacement selon l'axe x
 * @param dy déplacement selon l'axe y
 * @param dz déplacement selon l'axe z
 * @return rien
*/
void Point3d::move(float dx, float dy, float dz)
{
    Point2d::move(dx, dy);
    z_ += dz;
}


/**
 * @brief calcule la distance entre 2 points
 * 
 * @param p le point par rapport auquel on calcule la distance
 * @return la distance entre les 2 points
*/
float Point3d::distance(const Point3d &p) const 
{
    float deux_d = Point2d::distance(p);
    float carre = pow(deux_d, 2);

    return sqrt(carre + pow((p.z_ - z_), 2));
}





/**
 * -------------------------------------------------------------------------------------------------------------
 * -------------------------------------------------- Opérateurs externes --------------------------------------------------
 * -------------------------------------------------------------------------------------------------------------
*/

/**
 * @brief affichage du point sur un flux de sortie
 * 
 * @param flux un objet std::ostream qui permet l'affichage
 * @param p le point à afficher 
 * @return un pointeur vers le flux de sortie std::ostream après l'affichage
 *
*/
std::ostream & operator<<(std::ostream &flux, const Point2d &p)
{
    flux << "(" << p.getX() << "," << p.getY() << ")";
    return flux;
}


/**
 * @brief affichage du point sur un flux de sortie
 * 
 * @param flux un objet std::ostream qui permet l'affichage
 * @param p le point à afficher 
 * @return un pointeur vers le flux de sortie std::ostream après l'affichage
 *
*/
std::ostream & operator<<(std::ostream &flux, const Point3d &p)
{
    flux << "(" << p.getX() << "," << p.getY() << "," << p.getZ() << ")";
    return flux;
}


