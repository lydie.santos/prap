/** 
 * \file main.cpp
 * 
 * g++ -g -Wall -Wextra -o projet *.cpp $(pkg-config --cflags --libs sdl2)
*/

#include <stdio.h>
#include <stdlib.h>
#include <iostream> //for the use of input and ouput stream

#include "../headers/point.h"
#include "../headers/triangle.h"
#include "../headers/sdl.h"
#include "event.cpp"


int main()
{
    try 
    {
        if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
        {
            // SDL n’est pas initialisé, on sort
            return -1;
        }
        

        std::string titre = "Ma fenêtre";
        position pos{SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED};
        size s{640,480};
        Sdl::init_window(titre, pos, s);


        Point2d p1 = Point2d(10,470);
        Point2d p2 = Point2d(320,10);
        Point2d p3 = Point2d(630,470);



        Triangle2d t = Triangle2d(p1, p2, p3);
        /*SDL_Color couleur = {255, 0, 0, 255};
        SDL_SetRenderDrawBlendMode(reinterpret_cast<SDL_Renderer*>(Sdl::getRen()), SDL_BLENDMODE_BLEND);
        t.remplissage(reinterpret_cast<SDL_Renderer*>(Sdl::getRen()), couleur);
*/
        Sdl::Draw(t);
        Sdl::present();
        
        Point3d p4(0,0,0);
        Point3d p5(0,0,1);
        Point3d p6(0,1,0);
        Point3d p7(0,1,1);
        Point3d p8(1,0,0);
        Point3d p9(1,0,1);
        Point3d p10(1,1,0);
        Point3d p11(1,1,1);
        Quad q[6];
        q[0]=Quad(Triangle3d(p4,p6,p10), Triangle3d(p4,p10,p8));
        q[1]=Quad(Triangle3d(p8,p10,p11), Triangle3d(p8,p11,p9));
        q[2]=Quad(Triangle3d(p9,p11,p7), Triangle3d(p9,p7,p5));
        q[3]=Quad(Triangle3d(p5,p7,p6), Triangle3d(p5,p6,p4));
        q[4]=Quad(Triangle3d(p6,p7,p11), Triangle3d(p6,p11,p10));
        q[5]=Quad(Triangle3d(p9,p5,p4), Triangle3d(p9,p4,p8));

        Pave3d p=Pave3d(q);

        Sdl::Draw(p);
        Sdl::present();

        Sdl::show();

        Sdl::add_event(ExitEvent::create()); 

        Sdl::run();

        Sdl::quit();
    }
    catch (const std::exception & exep)
    {
        std::cout << "Erreur : La fenêtre n'a pas été correctement initialisée" << std::endl;
    }

    /**
    //Initialisation de SDL
    if(SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        //SDL n'est pas initialisé, on sort
        return -1;
    }





    Point2d p1(1,2);
    std::cout << p1 << std::endl;


    Point2d p2(1,2);
    std::cout << p2 << std::endl;

    std::cout << std::boolalpha << (p1==p2) << std::endl;


    Point3d p3(1,2,3);
    Point3d p4(1,2,3);
    std::cout << std::boolalpha << (p3==p4) << std::endl;
    Point3d p5(1,2,5);
    std::cout << std::boolalpha << (p3==p5) << std::endl;


    Point3d p6(2.5,4.8,10);
    std::cout << p3.distance(p6) << std::endl;

    std::cout << p6 << std::endl;


    



    Point2d p10(1,2);
    Point2d p11(5,-2);

    std::cout << "Traingle Rectangle avec (1,2) et (5,-2)" << std::endl;;
    Triangle2d t=Triangle2d::triangleRectangle(p10, p11);

    std::cout << "point3 :" << t.getP1() << t.getP2() << t.getP3() << std::endl;

    Point2d p12;
    p12=p1;
    std::cout << "point12 : " << p12 << std::endl;

    */

    return 0;
}