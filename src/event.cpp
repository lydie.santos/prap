/**
 * @file event.cpp
*/

#include "../headers/event.h"
#include "../headers/sdl.h"



/**
 * @brief événement de sortit : quand 'q' est pressé, on ferme la fenêtre courante
*/
struct ExitEvent : public Event
{
    void callback(int keysym) const 
    {
        if (keysym == 'q')
        {
            Sdl::exit_run();
        }
    }


    static std::shared_ptr<Event> create()
    {
        ExitEvent* e = new ExitEvent();
        return std::shared_ptr<ExitEvent>(e);
    }
};