/** 
* \file pave3d.cpp
* This file contains the declaration of the pave3d class
*/

#include "../headers/pave3d.h"

// TODO vérifier les constructeurs 

/**
 * @brief Constructeur par défaut
*/
Pave3d::Pave3d()
{
    // allocation dynamique d'un tableau de 6 caractères
    q_=new Quad[6];

    // on initialise les quadrilatères avec le constructeur par défaut
    for (int i=0; i<6; i++)
    {
        q_[i]=Quad();
    }

}


/**
 * @brief Constructeur valué
 * 
 * @param q le pointeur vers les quadrilatères
*/
Pave3d::Pave3d(Quad* q)
{
    q_=new Quad[6];
    for (int i=0;i<6;i++)
    {
        q_[i]=q[i];
    }
}


/**
 * @brief Constructeur de copie
 * 
 * @param p le pavé à copier
*/
Pave3d::Pave3d(const Pave3d &p)
{
    q_=new Quad[6];
    for (int i=0;i<6;i++)
    {
        q_[i]=p.q_[i];
    }
}


/**
 * Destructeur
*/
Pave3d::~Pave3d()
{
    delete[] q_;
}


// ---------------------------------------------- Getters & Setters ----------------------------------------------


/**
 * @brief Getter du premier quadrilatère du pavé
 * 
 * @return Le pointeur vers le tableau de quadrilatères
*/
Quad* Pave3d::getQ() const
{
    return q_;
}


/**
 * @brief Setter du premier quadrilatère du pavé
 * 
 * @param q un pointeur vers un tableau de 6 quadrilatère composant le pave
 * @return rien
*/
void Pave3d::setQ(Quad* q)
{
    for (int i=0; i<6; i++)
    {
        q_[i] = q[i];
    }
}





// ---------------------------------------------- Surcharge d'opérateurs ----------------------------------------------

/**
 * @brief Teste l'égalité entre 2 pavés
 * 
 * @param p le pavé à comparer
 * @return true si les 2 pavés sont égaux, false sinon
*/
bool Pave3d::operator==(const Pave3d &p) const
{
    for (int i =0; i<6; i++)
    {
        if (q_[i] != p[i]) return false;
    }
    return true;
}



/**
 * @brief Teste la différence entre 2 pavés
 * 
 * @param p le pavé à comparer
 * @return true si les 2 pavés sont différents, false sinon
*/
bool Pave3d::operator!=(const Pave3d &p) const
{
    for (int i =0; i<6; i++)
    {
        if (q_[i] != p[i]) return true;
    }
    return false;
}



/**
 * @brief Affecte la valeur d'un quadrilatère
 * 
 * @param p le pavé à affecter
 * @return rien
*/
Pave3d & Pave3d::operator=(const Pave3d &p)
{
    if (this != &p)
    {
        for (int i=0; i<6; i++)
        {
            q_[i] = p[i];
        }
    }
    return *this;
}


/**
* @brief Retourne le i-ème quadrilatère du pavé
* 
* @param i indice du quadrilatère à retourner
*/
Quad Pave3d::operator[](int i) const
{
    return q_[i];
}





// ---------------------------------------------- Autres méthodes ----------------------------------------------

/**
 * @brief décale le pavé
 * 
 * @param dx déplacement selon l'axe x
 * @param dy déplacement selon l'axe y
 * @param dz déplacement selon l'axe z
 * @return rien
*/
void Pave3d::move(float dx, float dy, float dz)
{
    for (int i=0; i<6; i++)
    {
        q_->move(dx,dy,dz);
    }
}