/** 
* \file vector3.cpp
* This file contains the declaration of the vector3 class
*/

#include "../headers/vector3.h"
#include <cmath>
#include <stdexcept>





// ---------------------------------------------- Constructeurs & Destructeur ----------------------------------------------

Vector3::Vector3(float x, float y, float z)
{
    x_ = x;
    y_ = y;
    z_ = z;
}


Vector3::Vector3(const Vector3 &v)
{
    x_ = v.x_;
    y_ = v.y_;
    z_ = v.z_;
}



        

// ---------------------------------------------- Getters & Setters ----------------------------------------------

float Vector3::getX()
{
    return x_;
}


float Vector3::getY()
{
    return y_;
}


float Vector3::getZ()
{
    return z_;
}


void Vector3::setX(float x)
{
    x_ = x;
}


void Vector3::setY(float y)
{
    y_ = y;
}


void Vector3::setZ(float z)
{
    z_ = z;
}





// ---------------------------------------------- Surcharge d'opérateurs ----------------------------------------------

bool Vector3::operator==(const Vector3 &v) const
{
    return (x_ == v.x_ && y_ == v.y_ && z_ == v.z_);
}


bool Vector3::operator!=(const Vector3 &v) const
{
    return !(*this==v);
}

Vector3 & Vector3::operator=(const Vector3 &v)
{
    if (this != &v)
    {
        x_ = v.x_;
        y_ = v.y_;
        z_ = v.z_;
    }
    return *this;
}


Vector3 & Vector3::operator+=(const Vector3 &v)
{
    x_ += v.x_;
    y_ += v.y_;
    z_ += v.z_;

    return *this;
}


Vector3 & Vector3::operator-=(const Vector3 &v)
{
    x_ -= v.x_;
    y_ -= v.y_;
    z_ -= v.z_;

    return *this;
}


Vector3 & Vector3::operator*=(const float &f)
{
    x_ *= f;
    y_ *= f;
    z_ *= f;

    return *this;
}


/**
 * @brief Divise un vecteur par un coefficient
 * 
 * @param f le coefficient multiplicateur (non null)
 * @return le nouveau vecteur
*/
Vector3 & Vector3::operator/=(const float &f)
{
    if (f == 0) 
    {
        throw std::invalid_argument("Division by zero");
    }

    x_ /= f;
    y_ /= f;
    z_ /= f;

    return *this;
}





// ---------------------------------------------- Autres méthodes ----------------------------------------------

/**
 * @brief Normalise le vecteur courant
*/
void Vector3::normalise()
{
    float length = sqrt(x_*x_ + y_*y_ + z_*z_);

    if(length != 0)
    {
        x_ /= length;
        y_ /= length;
        z_ /= length;
    }
}


/**
 * @brief Calcule la norme du vecteur
 * 
 * @return la norme du vecteur courant
*/
float Vector3::magnitude()
{
    return sqrt(x_*x_ + y_*y_ + z_*z_);
}


/**
 * @brief Effectur le produit scalaire entre 2 vecteurs
 * 
 * @param v l'autre vecteur pour le produit scalaire
 * @return le produit scalaire entre le vecteur courant et le paramètre
*/
float Vector3::dotProduct(const Vector3& v) const
{
    return x_*v.x_ + y_*v.y_ + z_*v.z_;
}


/**
 * @brief Effectur le produit vectoriel entre 2 vecteurs
 * 
 * @param v l'autre vecteur pour le produit vectoriel
 * @return le produit vectoriel entre le vecteur courant et le paramètre
*/
Vector3 Vector3::crossProduct(const Vector3& v) const
{
    float x = y_*v.z_ - z_*v.y_;
    float y = z_*v.x_ - x_*v.z_;
    float z = x_*v.y_ - y_*v.x_;

    return Vector3(x, y, z);
}