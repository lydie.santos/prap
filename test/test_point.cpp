/**
 * @file test_point.cpp
 * Test des classes Point2d et Point3d
 * 
 * compilation : g++ -g -Wall -Wextra -o tests test_point.cpp ../src/point.cpp $(pkg-config --cflags --libs sdl2)
*/

#include "../headers/point.h"


int main()
{
    /* ---------------------------------------------- Tests Point2d ---------------------------------------------- */

    printf("TESTS 2D\n\n");

    Point2d p1 = Point2d(2, 2);
    Point2d p2 = Point2d(3, 4);
    Point2d p3 = Point2d(p1);
    Point2d p4 = Point2d();

    printf("constructeur par défaut : %d\n", (p4.getX() == 0) && (p4.getY() == 0));
    printf("constructeur de copie : %d\n", p3 == p1);

    printf(" != : %d\n", p1 != p2);
    p4 = p2;
    printf(" = : %d\n", p4==p2);
    

    p1.setX(10);
    printf("getter et setter : %d\n", p1.getX() == 10);

    p2.move(1.5, 0.4);
    printf("move : %d\n", (p2.getX() == (float)4.5) && (p2.getY() == (float)4.4));

    float dist = p1.distance(p2);
    printf("distance : %d\n", 6.000830 < dist  && dist < 6.00084); //pour les approximations





    /* ---------------------------------------------- Tests Point3d ---------------------------------------------- */

    printf("TESTS 3D\n\n");

    Point3d p5 = Point3d(2, 2, 2);
    Point3d p6 = Point3d(3, 4, 5);
    Point3d p7 = Point3d(p5);
    Point3d p8 = Point3d();
    Point3d p9 = Point3d(p1);

    printf("constructeur par défaut : %d\n", (p8.getX() == 0) && (p8.getY() == 0 && p8.getZ() == 0));
    printf("constructeur de copie : %d\n", p7 == p5);
    printf("constructeur de copie d'un point2d : %d\n", p1.getX() == p9.getX() && p1.getY() == p9.getY() && p9.getZ() == 0);

    printf(" != : %d\n", p5 != p6);
    p8 = p6;
    printf(" = : %d\n", p8==p6);

    p5.setZ(10);
    printf("getter et setter : %d\n", p5.getZ() == 10);

    p5.move(0.1, 1.7, 3);
    printf("move : %d\n", (p5.getX() == (float)2.1) && (p5.getY() == (float)3.7 && (p5.getZ() == 13)));

    float dist2 = p5.distance(p6);
    printf("distance : %d\n", 8.05605 < dist2  && dist2 < 8.05606); //pour les approximations





    return 0;
};