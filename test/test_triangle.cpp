/**
 * @file test_triangle.cpp
 * Test des classes Triangle2d et Triangle3d
 * 
 * compilation : g++ -g -Wall -Wextra -o tests test_triangle.cpp ../src/point.cpp ../src/triangle.cpp $(pkg-config --cflags --libs sdl2)
*/

#include "../headers/point.h"
#include "../headers/triangle.h"


int main()
{
    /* ---------------------------------------------- Tests Triangle2d ---------------------------------------------- */

    printf("TESTS 2D\n\n");

    Point2d p1 = Point2d(2, 2);
    Point2d p2 = Point2d(3, 4);
    Point2d p3 = Point2d(7, 7);

    Triangle2d t1 = Triangle2d();
    Triangle2d t2 = Triangle2d(p1, p2, p3);
    Triangle2d t3 = Triangle2d(t2);

    printf("constructeur par défaut : %d\n", (t1.getP1() == Point2d()) && (t1.getP2() == Point2d()) && (t1.getP3() == Point2d()));
    printf("constructeur valué : %d\n", (t2.getP1() == p1) && (t2.getP2() == p2) && (t2.getP3() == p3));
    printf("constructeur de copie : %d\n", t3 == t2);

    printf(" != : %d\n", t1 != t2);
    t1 = t2;
    printf(" = : %d\n", t1 == t2);
    

    t2.setP2(p3);
    printf("getter et setter : %d\n", t2.getP2() == p3);


    
    Triangle2d t_rect = Triangle2d::triangleRectangle(p1,p2);
    printf("affichage des coordonnées du troisième point du triangle rectangle : %f, %f\n", (t_rect.getP3()).getX(), (t_rect.getP3()).getY());
    printf("on vérifie si le triangle est rectangle : %d\n", t_rect.isRectangle());
    
    Triangle2d t_move=t2;
    t_move.move(0.5,1.8);
    p1.move(0.5,1.8);
    p2.move(0.5,1.8);
    p3.move(0.5,1.8);
    printf("move : %d\n", (t_move.getP1()==p1) && (t_move.getP2()==p3) && (t_move.getP3()==p3));

    
    //TODO : test distance
    float dist=t1.distance(t2);
    printf("distance %f\n", dist);




    /* ---------------------------------------------- Tests Triangle3d ---------------------------------------------- */

    printf("TESTS 3D\n\n");

    Point3d p4 = Point3d(2, 2, 2);
    Point3d p5 = Point3d(3, 4, 5);
    Point3d p6 = Point3d(7, 7, 8);

    Triangle3d t4 = Triangle3d();
    Triangle3d t5 = Triangle3d(p4, p5, p6);
    Triangle3d t6 = Triangle3d(t5);
    Triangle3d t7 = Triangle3d(t2);

    printf("constructeur par défaut : %d\n", (t4.getP1() == Point3d()) && (t4.getP2() == Point3d()) && (t4.getP3() == Point3d()));
    printf("constructeur valué : %d\n", (t5.getP1() == p4) && (t5.getP2() == p5) && (t5.getP3() == p6));
    printf("constructeur de copie : %d\n", t6 == t5);
    printf("constructeur de copie 2d : %d\n", t7.getP1() == Point3d(t2.getP1()) && t7.getP2() == Point3d(t2.getP2()) && t7.getP3() == Point3d(t2.getP3()));

    printf(" != : %d\n", t4 != t5);
    t4 = t5;
    printf(" = : %d\n", t4 == t5);
    

    t5.setP2(p6);
    printf("getter et setter : %d\n", t5.getP2() == p6);

    Triangle3d t_rect2 = Triangle3d::triangleRectangle(p4,p5);
    printf("affichage des coordonnées du troisième point du triangle rectangle : %f, %f, %f\n", (t_rect2.getP3()).getX(), (t_rect2.getP3()).getY(), (t_rect2.getP3()).getZ());
    printf("on vérifie si le triangle est rectangle : %d\n", t_rect2.isRectangle());
    

    Triangle3d t_move2 = t6;
    t_move2.move(3.0, 2.8, 1.2);
    p4.move(3.0, 2.8, 1.2);
    p5.move(3.0, 2.8, 1.2);
    p6.move(3.0, 2.8, 1.2);
    printf("move : %d\n", (t_move2.getP3()==p6) && (t_move2.getP2()==p5) && (t_move2.getP3()==p6));

    //TODO : test distance



    return 0;
};