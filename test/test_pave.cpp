/**
 * @file test_pave.cpp
 * Test de la classe Pave3d
 * 
 * compilation : g++ -g -Wall -Wextra -o tests test_pave.cpp ../src/point.cpp ../src/triangle.cpp ../src/quad3d.cpp ../src/pave3d.cpp $(pkg-config --cflags --libs sdl2)
*/

#include "../headers/point.h"
#include "../headers/triangle.h"
#include "../headers/quad3d.h"
#include "../headers/pave3d.h"


int main()
{
    /* ---------------------------------------------- Tests Pave3d ---------------------------------------------- */
    Point3d p1 = Point3d(2, 2, 2);
    Point3d p2 = Point3d(3, 4, 5);
    Point3d p3 = Point3d(7, 7, 8);

    Triangle3d t1 = Triangle3d(p1,p2,p3);
    Triangle3d t2 = Triangle3d();

    Quad q1 = Quad();
    Quad q2 = Quad(t1,t2);
    Quad q3 = Quad(q2);
    Quad q[6];
    q[0]=q1;
    q[1]=q2;
    q[2]=q3; 
    q[3]=q1;
    q[4]=q2;
    q[5]=q3;

    Pave3d pave1 = Pave3d();
    Pave3d pave2 = Pave3d(q);
    Pave3d pave3 = Pave3d(pave2);
    
    
    printf("constructeur par défaut : %d\n", (pave1.getQ()[0] == Quad()) && (pave1.getQ()[1] == Quad()) 
    && (pave1.getQ()[2] == Quad()) && (pave1.getQ()[3] == Quad()) && (pave1.getQ()[4] == Quad()) 
    && (pave1.getQ()[5] == Quad()));

    printf("constructeur valué : %d\n", (pave2.getQ()[0] == q1) && (pave2.getQ()[1] == q2) 
    && (pave2.getQ()[2] == q3) && (pave2.getQ()[3] == q1) && (pave2.getQ()[4] == q2) 
    && (pave2.getQ()[5] == q3));

    printf("constructeur de copie : %d\n", pave3 == pave2);

    // TODO destructeur 


    printf(" != : %d\n", pave1 != pave2);
    Pave3d pave4 = pave1;
    printf(" = : %d\n", pave4 == pave1);
    
    Quad q4[6];
    for (int i=0; i<6; i++)
    {
        q4[i]=q[i];
    }
    q4[0]=q2;

    pave3.setQ(q4);
    printf("getter et setter : %d\n", pave3.getQ() == q4);
    // TODO le test ne passe pas

    return 0;

}