/**
 * @file test_quad.cpp
 * Test de la classe Quad
 * 
 * compilation : g++ -g -Wall -Wextra -o tests test_quad.cpp ../src/point.cpp ../src/triangle.cpp ../src/quad3d.cpp $(pkg-config --cflags --libs sdl2)
*/

#include "../headers/point.h"
#include "../headers/triangle.h"
#include "../headers/quad3d.h"


int main()
{
    /* ---------------------------------------------- Tests Quad ---------------------------------------------- */

    Point3d p1 = Point3d(2, 2, 2);
    Point3d p2 = Point3d(3, 4, 5);
    Point3d p3 = Point3d(7, 7, 8);

    Triangle3d t1 = Triangle3d(p1,p2,p3);
    Triangle3d t2 = Triangle3d();

    Quad q1 = Quad();
    Quad q2 = Quad(t1,t2);
    Quad q3 = Quad(q2);



    printf("constructeur par défaut : %d\n", (q1.getT1() == Triangle3d()) && (q1.getT2() == Triangle3d()));
    printf("constructeur valué : %d\n", (q2.getT1() == t1) && (q2.getT2() == t2));
    printf("constructeur de copie : %d\n", q3 == q2);


    printf(" != : %d\n", q1 != q3);
    Quad q4 = q1;
    printf(" = : %d\n", q4 == q1);

    q3.setT1(t2);
    printf("getter et setter : %d\n", q3.getT1() == t2);

    Triangle3d t_rect = Triangle3d::triangleRectangle(p1,p2);
    
    Quad q5=quadRectangle(t_rect);
    printf("points du quad : \n t1 : (%f, %f, %f), (%f, %f, %f), (%f, %f, %f) \n t2 : (%f, %f, %f), (%f, %f, %f), (%f, %f, %f)\n", 
    q5.getT1().getP1().getX(), q5.getT1().getP1().getY(), q5.getT1().getP1().getZ(), 
    q5.getT1().getP2().getX(), q5.getT1().getP2().getY(), q5.getT1().getP2().getZ(), 
    q5.getT1().getP3().getX(), q5.getT1().getP3().getY(), q5.getT1().getP3().getZ(),
    q5.getT2().getP1().getX(), q5.getT2().getP1().getY(), q5.getT2().getP1().getZ(), 
    q5.getT2().getP2().getX(), q5.getT2().getP2().getY(), q5.getT2().getP2().getZ(), 
    q5.getT2().getP3().getX(), q5.getT2().getP3().getY(), q5.getT2().getP3().getZ());

    printf("rectangle %d\n", q5.isRectangle());

    Quad q_move=q5;
    q_move.move(0.5,3.9,1.2);
    Triangle3d t3=q5.getT1();
    Triangle3d t4=q5.getT2();
    t3.move(0.5,3.9,1.2);
    t4.move(0.5,3.9,1.2);
    printf("move : %d\n", (q_move.getT1()==t3) && (q_move.getT2()==t4));


    
    return 0;
};