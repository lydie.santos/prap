/** 
* \file point.h
* This file contains the declaration of the point2d and point3d class
*/

#ifndef POINT_H
#define POINT_H

#include <iostream>




class Point2d
{
    protected:
        /* un point 2D a 2 coordonnées : x et y */
        float x_;
        float y_;


    public:
        // ---------------------------------------------- Constructeurs & Destructeur ----------------------------------------------

        /**
         * @brief Constructeur par défaut et valué
         * 
         * @param x première coordonnée du point
         * @param y deuxième coordonnée du point
        */
        Point2d(float x = 0, float y = 0);

        /**
         * @brief Constructeur de copie
         * 
         * @param p le point à copier
        */
        Point2d(const Point2d &p);





        // ---------------------------------------------- Getters & Setters ----------------------------------------------
        /**
         * @brief Getter de la première coordonnée du point
         *          
         * @return La coordonnée x du point
        */
        float getX() const;

        
        /**
         * @brief Getter de la deuxième coordonnée du point
         *          
         * @return La coordonnée y du point
        */
        float getY() const;

        
         /**
         * @brief Setter de la première coordonnée du point
         *   
         * @param x la coordonnée à fixer comme première coordonnée du point       
         * @return rien
        */
        void setX(float x);


         /**
         * @brief Setter de la deuxième coordonnée du point
         *   
         * @param y la coordonnée à fixer comme deuxième coordonnée du point       
         * @return rien
        */
        void setY(float y);



        // ---------------------------------------------- Surcharge d'opérateurs ----------------------------------------------

        /**
         * @brief Teste l'égalité entre 2 points
         * 
         * @param t le point à comparer
         * @return true si les 2 points sont égaux, false sinon
        */
        bool operator==(const Point2d &p) const;


        /**
         * @brief Teste la différence entre 2 points
         * 
         * @param t le point à comparer
         * @return true si les 2 points ne sont pas égaux, false sinon
        */
        bool operator!=(const Point2d &p) const;


        /**
         * @brief affecte à un point un autre point
         * 
         * @param p le point à affecter
         * @return le nouveau point
        */
        Point2d & operator=(const Point2d &p);


        /**
         * @brief Multiplie le point courant par un coefficient
         * 
         * @param f le coefficient
         * @return le nouveau point
        */
        Point2d & operator*=(const float &f);


        // ---------------------------------------------- Autres méthodes ----------------------------------------------
        
        /**
         * @brief décale le point
         * 
         * @param dx déplacement selon l'axe x
         * @param dy déplacement selon l'axe y
         * @return rien
        */
        void move(float dx, float dy);


        /**
         * @brief calcule la distance entre 2 points
         * 
         * @param p le point par rapport auquel on calcule la distance
         * @return la distance entre les 2 points
        */
        float distance(const Point2d &p) const;



};



class Point3d : public Point2d
{
    protected:
        float z_; //un point 3D a une coordonnée de plus : z
    


    public:
        // ---------------------------------------------- Constructeurs & Destructeur ----------------------------------------------

        /**
         * @brief Constructeur par défaut et valué
         * 
         * @param x première coordonnée du point
         * @param y deuxième coordonnée du point
         * @param z troisième coordonnée du point
         */
         
        Point3d(float x = 0, float y = 0, float z = 0);


        /**
         * @brief Constructeur de copie
         * 
         * @param p le point à copier en 2D
        */
        Point3d(const Point2d &p);


        /**
         * @brief Constructeur de copie
         * 
         * @param p le point à copier en 3D
        */
        Point3d(const Point3d &p);
        




        // ---------------------------------------------- Getters & Setters ----------------------------------------------

        /**
         * @brief Getter de la troisième coordonnée du point 
         * 
         * @return La troisième coordonnée (z) constituant le point
        */
        float getZ() const;


        /**
         * @brief Setter de la trosième coordonnée du point
         *
         * @param z la coordonnée à fixer comme troisième coordonnée du point
         * @return rien
        */
        void setZ(float z);




        // ---------------------------------------------- Surcharge d'opérateurs ----------------------------------------------

        /**
         * @brief Teste l'égalité entre 2 points
         * 
         * @param p le point à comparer
         * @return true si les 2 points sont égaux, false sinon
        */
        bool operator==(const Point3d &p) const;


        /**
         * @brief Teste la différence entre 2 points
         * 
         * @param t le point à comparer
         * @return true si les 2 points ne sont pas égaux, false sinon
        */
        bool operator!=(const Point3d &p) const;


        /**
         * @brief affecte à un point un autre point
         * 
         * @param p le point à affecter
         * @return le nouveau point
        */
        Point3d & operator=(const Point3d &p);


        /**
         * @brief Multiplie le point courant par un coefficient
         * 
         * @param f le coefficient
         * @return le nouveau point
        */
        Point3d & operator*=(const float &f);


        // ---------------------------------------------- Autres Méthodes ----------------------------------------------

        /**
         * @brief décale le point
         * 
         * @param dx déplacement selon l'axe x
         * @param dy déplacement selon l'axe y
         * @param dz déplacement selon l'axe z
         * @return rien
        */
        void move(float dx, float dy, float dz);


        /**
         * @brief calcule la distance entre 2 points
         * 
         * @param p le point par rapport auquel on calcule la distance
         * @return la distance entre les 2 points
        */
        float distance(const Point3d &p) const;



};

// ---------------------------------------------- Opérateurs externes ----------------------------------------------


/**
 * @brief affichage du point sur un flux de sortie
 * 
 * @param flux un objet std::ostream qui permet l'affichage
 * @param p le point à afficher 
 * @return un pointeur vers le flux de sortie std::ostream après l'affichage
 *
*/
std::ostream & operator<<(std::ostream &flux, const Point2d &p);

/**
 * @brief affichage du point sur un flux de sortie
 * 
 * @param flux un objet std::ostream qui permet l'affichage
 * @param p le point à afficher 
 * @return un pointeur vers le flux de sortie std::ostream après l'affichage
 *
*/
std::ostream & operator<<(std::ostream &flux, const Point3d &p);




#endif