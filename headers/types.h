/**
 * @file types.hpp
*/

#ifndef TYPES_H
#define TYPES_H

#include "event.h"
#include <array>
#include <memory> //shared_ptr



/**
 * Position
 * -> Définie par 2 entiers : le premier désigne la position en x (abscisse) et le second, la
 *    position en y (ordonnée)
*/
typedef std::array<int,2> position;

/**
 * Taille
 * -> Définie par 2 entiers : le premier désigne la largeur et le second la hauteur
*/
typedef std::array<int,2> size;

/**
 * Pointeur intelligent d'événements
*/
typedef std::shared_ptr<Event> pevent;



#endif