/** 
* \file quad3d.h
* This file contains the declaration of the quad3d class
*/

#ifndef QUAD3D_H
#define QUAD3D_H

#include "triangle.h"


class Quad
{
    private : 
        //un quadrilatère est composé de 2 triangles ayant deux points (un côté) en commun
            // On admet que ce sont les points p1_ et p2_ de chaque triangle qui sont respectivement en commun
        Triangle3d t1_;
        Triangle3d t2_;


    public:
        // ---------------------------------------------- Constructeurs & Destructeur ----------------------------------------------

        /**
         * @brief Constructeur par défaut et valué
         * 
         * @param t1 premier triangle constituant le quadrilatère
         * @param t2 deuxième triangle constitant le quadrilatère
        */
        Quad(Triangle3d t1 = Triangle3d(), Triangle3d t2 = Triangle3d());


        /**
         * @brief Constructeur de copie
         * 
         * @param q le quadrilatère à copier
        */
        Quad(const Quad &q);





        // ---------------------------------------------- Getters & Setters ----------------------------------------------
        
        /**
         * @brief Getter du premier triangle composant le quadrilatère
         * 
         * @return le premier triangle constituant le quadrilatère
        */
        Triangle3d getT1() const;


        /**
         * @brief Getter du deuxième triangle composant le quadrilatère
         * 
         * @return le deuxième triangle constituant le quadrilatère
        */
        Triangle3d getT2() const;
        


        /**
         * @brief Setter du premier triangle du quadrilatère
         * 
         * @param t le triangle à fixer comme premier triangle du quadrilatère
         * @return rien
        */
        void setT1(Triangle3d t);


        /**
         * @brief Setter du deuxième triangle du quadrilatère
         * 
         * @param t le triangle à fixer comme deuxième triangle du quadrilatère
         * @return rien
        */
        void setT2(Triangle3d t);



        // ---------------------------------------------- Surcharge d'opérateurs ----------------------------------------------

        /**
         * @brief Teste l'égalité entre 2 quadrilatères
         * 
         * @param q le quadrilatère à comparer
         * @return true si les 2 quadrilatères sont égaux, false sinon
        */
        bool operator==(const Quad &q) const;


        /**
         * @brief Teste la différence entre 2 quadrilatères
         * 
         * @param q le quadrilatère à comparer
         * @return true si les 2 quadrilatères ne sont pas égaux, false sinon
        */
        bool operator!=(const Quad &q) const;


        /**
         * @brief Affecte la valeur d'un quadrilatère
         * 
         * @param q quadrilatère que l'on veut affecter
         * @return rien
        */
        Quad & operator=(const Quad &q);





        // ---------------------------------------------- Autres méthodes ----------------------------------------------

        /**
         * @brief fonction qui teste si le quadrilatère est un rectangle
         *
         * @return true si le quadrilatère est un rectangle, false sinon
         */
        bool isRectangle();


        /**
         * @brief décale le quadrilatère
         * 
         * @param dx déplacement selon l'axe x
         * @param dy déplacement selon l'axe y
         * @param dz déplacement selon l'axe z
         * @return rien
        */
        void move(float dx, float dy, float dz);
};


// ---------------------------------------------- Méthodes externes ----------------------------------------------

/**
 * @brief Fonction qui créée un rectangle
 * 
 * @param t triangle rectangle 
 * @return le rectangle créé
*/
Quad quadRectangle(Triangle3d t);


#endif