/**
 * @file event.h
*/

#ifndef EVENT_H
#define EVENT_H

#include <SDL2/SDL.h>
#include <memory>



struct Event 
{   
    virtual void callback(int keysym) const = 0;

    static std::shared_ptr<Event> create();
};




#endif