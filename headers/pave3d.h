/** 
* \file pave3d.h
* This file contains the declaration of the pave3d class
*/

#ifndef PAVE3D_H
#define PAVE3D_H
#include "quad3d.h"





class Pave3d
{
    private : 
        // un pavé est composé de 6 quadrilatères
        // on choisit d'utiliser un pointeur vers un tableau de 6 éléments
        Quad* q_;


    public : 
        // ---------------------------------------------- Constructeurs & Destructeur ----------------------------------------------

        /**
         * @brief Constructeur par défaut
        */
        Pave3d();


        /**
         * @brief Constructeur valué
         * 
         * @param q le pointeur vers les quadrilatères
        */
        Pave3d(Quad* q);
        
        
        /**
         * @brief Constructeur de copie
         * 
         * @param p le pavé à copier
        */
        Pave3d(const Pave3d &p);
        
        
        /**
         * Destructeur
        */
        ~Pave3d();


        // ---------------------------------------------- Getters & Setters ----------------------------------------------


        /**
         * @brief Getter du premier quadrilatère du pavé
         * 
         * @return Le pointeur vers le tableau de quadrilatères
        */
        Quad* getQ() const;


        /**
         * @brief Setter du premier quadrilatère du pavé
         * 
         * @param q un pointeur vers un tableau de 6 quadrilatère composant le pave
         * @return rien
        */
        void setQ(Quad* q);





        // ---------------------------------------------- Surcharge d'opérateurs ----------------------------------------------

        /**
         * @brief Teste l'égalité entre 2 pavés
         * 
         * @param p le pavé à comparer
         * @return true si les 2 pavés sont égaux, false sinon
        */
        bool operator==(const Pave3d &p) const;


        /**
         * @brief Teste la différence entre 2 pavés
         * 
         * @param p le pavé à comparer
         * @return true si les 2 pavés sont différents, false sinon
        */
        bool operator!=(const Pave3d &p) const;

        /**
         * @brief Affecte la valeur d'un quadrilatère
         * 
         * @param p le pavé à affecter
         * @return rine
        */
       Pave3d & operator=(const Pave3d &p);


       /**
        * @brief Retourne le i-ème quadrilatère du pavé
        * 
        * @param i indice du quadrilatère à retourner
       */
        Quad operator[](int i) const;





        // ---------------------------------------------- Autres méthodes ----------------------------------------------

        /**
         * @brief décale le pavé
         * 
         * @param dx déplacement selon l'axe x
         * @param dy déplacement selon l'axe y
         * @param dz déplacement selon l'axe z
         * @return rien
        */
        void move(float dx, float dy, float dz);
};



#endif