/** 
* \file scene3d.h
* This file contains the declaration of the scene3d class
*/

#ifndef SCENE3D_H
#define SCENE3D_H

#include "sphere3d.h"
#include "pave3d.h"





class Scene3d
{
    private:
        Pave3d cube_;
        Sphere sphere_;
    
    public:
        // ----------------------------------------- Constructeurs & Destructeur -----------------------------------------
        
        /**
         * @brief Constructeur par défaut et valué
         * 
         * @param cube le cube composant la scène
         * @param sphere la sphère composant la scène
        */
        Scene3d(Pave3d cube = Pave3d(), Sphere sphere = Sphere());


        /**
         * @brief Constructeur de copie
         * 
         * @param scene la scène à copier
        */
        Scene3d(const Scene3d &scene);


        // ---------------------------------------------- Getters & Setters ----------------------------------------------

        /**
         * @brief Getter du cube composant la scène
         * 
         * @return le cube composant la scène
        */
        Pave3d getCube();


        /**
         * @brief Getter de la sphère composant la scène
         * 
         * @return la sphère composant la scène
        */
        Sphere getSphere();


        /**
         * @brief Setter du cube composant la scène
         * 
         * @param cube le cube à instancier comme cube de la scène
         * @return rien
        */
        void setCube(Pave3d cube);


        /**
         * @brief Setter de la sphère composant la scène
         * 
         * @param sphere la sphère à instancier comme sphère de la scène
         * @return rien
        */
        void setSphere(Sphere sphere);



        // ---------------------------------------------- Autres méthodes ----------------------------------------------

};


#endif