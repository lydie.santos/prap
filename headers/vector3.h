/**
 * @file vector3.hpp
*/

#ifndef VECTOR3_H
#define VECTOR3_H





class Vector3
{
    private:
        //Les 3 éléments du vecteur
        float x_; 
        float y_;
        float z_;
    
    public:
        // ---------------------------------------------- Constructeurs & Destructeur ----------------------------------------------

        /**
         * @brief Constructeur par défaut et valué
         * 
         * @param x premier élément du vecteur
         * @param y deuxième élément du vecteur
         * @param z troisième élément du vecteur
        */
        Vector3(float x = 0, float y = 0, float z = 0);
        

        /**
         * @brief Constructeur de copie
         * 
         * @param t le triangle à copier
        */
        Vector3(const Vector3 &v);



                

        // ---------------------------------------------- Getters & Setters ----------------------------------------------

        /**
         * @brief Getter premier élément du vecteur
         * 
         * @return le premier élément du vecteur
        */
        float getX();


        /**
         * @brief Getter deuxième élément du vecteur
         * 
         * @return le deuxième élément du vecteur
        */
        float getY();


        /**
         * @brief Getter troisième élément du vecteur
         * 
         * @return le troisième élément du vecteur
        */
        float getZ();
        
        
        /**
         * @brief Setter du premier élément du vecteur
         * 
         * @param p le nouveau coefficient
         * @return rien
        */
        void setX(float x);


        /**
         * @brief Setter du deuxième élément du vecteur
         * 
         * @param y le nouveau coefficient
         * @return rien
        */
        void setY(float y);


        /**
         * @brief Setter du troisième élément du vecteur
         * 
         * @param z le nouveau coefficient
         * @return rien
        */
        void setZ(float z);





        // ---------------------------------------------- Surcharge d'opérateurs ----------------------------------------------

        /**
         * @brief Teste l'égalité entre 2 vecteurs
         * 
         * @param v le vecteur à comparer
         * @return true si les 2 vecteurs sont égaux, false sinon
        */
        bool operator==(const Vector3 &v) const;


        /**
         * @brief Teste la différence entre 2 vecteurs
         * 
         * @param v le vecteur à comparer
         * @return true si les 2 vecteurs ne sont pas égaux, false sinon
        */
        bool operator!=(const Vector3 &v) const;

        /**
         * @brief Affecte les coefficients d'un vecteur au vecteur courant
         * 
         * @param v le vecteur à affecter
         * @return l'objet courant
        */
        Vector3 & operator=(const Vector3 &v);

        /**
         * @brief Ajoute un vecteur au vecteur courant
         * 
         * @param v le vecteur à ajouter
         * @return la somme des deux vecteurs
        */
        Vector3 & operator+=(const Vector3 &v);


        /**
         * @brief Soustrait un vecteur au vecteur courant
         * 
         * @param v le vecteur à soustraire
         * @return la différence des deux vecteurs
        */
        Vector3 & operator-=(const Vector3 &v);

        /**
         * @brief Multiplie un vecteur par un coefficient
         * 
         * @param f le coefficient multiplicateur
         * @return le nouveau vecteur
        */
        Vector3 & operator*=(const float &f);


        /**
         * @brief Divise un vecteur par un coefficient
         * 
         * @param f le coefficient multiplicateur (non null)
         * @return le nouveau vecteur
        */
        Vector3 & operator/=(const float &f);





        // ---------------------------------------------- Autres méthodes ----------------------------------------------

        /**
         * @brief Normalise le vecteur courant
        */
        void normalise();


        /**
         * @brief Calcule la norme du vecteur
         * 
         * @return la norme du vecteur courant
        */
        float magnitude();


        /**
         * @brief Effectur le produit scalaire entre 2 vecteurs
         * 
         * @param v l'autre vecteur pour le produit scalaire
         * @return le produit scalaire entre le vecteur courant et le paramètre
        */
        float dotProduct(const Vector3& v) const;


        /**
         * @brief Effectur le produit vectoriel entre 2 vecteurs
         * 
         * @param v l'autre vecteur pour le produit vectoriel
         * @return le produit vectoriel entre le vecteur courant et le paramètre
        */
        Vector3 crossProduct(const Vector3& v) const;
};

#endif