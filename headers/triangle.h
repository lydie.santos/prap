/** 
* \file triangle.h
* This file contains the declaration of the Triangle3d and triangle3d class
*/

#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "./point.h"
#include <SDL2/SDL.h>






class Triangle2d
{
    private:
        // un triangle est composé de 3 points
        Point2d p1_;
        Point2d p2_;
        Point2d p3_;
    
        // couleur du triangle 
        SDL_Color color_;

    public:
        // ---------------------------------------------- Constructeurs & Destructeur ----------------------------------------------

        /**
         * @brief Constructeur par défaut et valué
         * 
         * @param p1 premier point constituant le triangle
         * @param p2 deuxième point constitant le triangle
         * @param p3 troisième point constitant le triangle
        */
        Triangle2d(const Point2d &p1 = Point2d(), const Point2d &p2 = Point2d(), const Point2d &p3 = Point2d());


        /**
         * @brief Constructeur de copie
         * 
         * @param t le triangle à copier
        */
        Triangle2d(const Triangle2d &t);



                

        // ---------------------------------------------- Getters & Setters ----------------------------------------------

        /**
         * @brief Getter du premier point du triangle
         * 
         * @return Le premier point constituant le triangle
        */
        Point2d getP1() const;


        /**
         * @brief Getter du deuxième point du triangle
         * 
         * @return Le deuxième point constituant le triangle
        */
        Point2d getP2() const;


        /**
         * @brief Getter du troisième point du triangle
         * 
         * @return Le troisième point constituant le triangle
        */
        Point2d getP3() const;
        

        /**
         * @brief Getter de la couleur du triangle
         * 
         * @return La couleur du triangle
        */
        SDL_Color getColor() const;
        
        
        /**
         * @brief Setter du premier point du triangle
         * 
         * @param p le point à poser comme premier point du triangle
         * @return rien
        */
        void setP1(Point2d p);


        /**
         * @brief Setter du deuxième point du triangle
         * 
         * @param p le point à poser comme deuxième point du triangle
         * @return rien
        */
        void setP2(Point2d p);


        /**
         * @brief Setter du troisième point du triangle
         * 
         * @param p le point à poser comme troisième point du triangle
         * @return rien
        */
        void setP3(Point2d p);


        /**
         * @brief Setter de la couleur du triangle
         * 
         * @param color couleur à donner au triangle
         * @return rien
        */
        void setColor(SDL_Color color);





        // ---------------------------------------------- Surcharge d'opérateurs ----------------------------------------------

        /**
         * @brief Teste l'égalité entre 2 triangles
         * 
         * @param t le triangle à comparer
         * @return true si les 2 triangles sont égaux, false sinon
        */
        bool operator==(const Triangle2d &t) const;


        /**
         * @brief Teste la différence entre 2 triangles
         * 
         * @param t le triangle à comparer
         * @return true si les 2 triangles ne sont pas égaux, false sinon
        */
        bool operator!=(const Triangle2d &t) const;


        /**
         * @brief affecte à un triangle un autre triangle
         * 
         * @param t le triangle à affecter
         * @return le nouveau triangle
        */
        Triangle2d & operator=(const Triangle2d &t);



        // ---------------------------------------------- Autres méthodes ----------------------------------------------

        /**
         * @brief Fonction qui créée un triangle rectangle. Elle prend en argument 2 points et place le troisième de façon 
         * à former un triangle rectangle
         * 
         * @param p1 Premier point du triangle
         * @param p2 Deuxième point du triangle
         * @return le triangle rectangle créé
        */
        static Triangle2d triangleRectangle(const Point2d &p1, const Point2d &p2); // Juste 2 points car le troisième sera créé pour qu'il soit rectangle
        
        
        /**
         * @brief fonction qui teste si le triangle est rectangle
         *
         * @return true si le triangle est rectangle, false sinon
         */
        bool isRectangle();
        
        
        /**
         * @brief décale le triangle
         * 
         * @param dx déplacement selon l'axe x
         * @param dy déplacement selon l'axe y
         * @return rien
        */
        void move(float dx, float dy);
        
        
        /**
         * @brief calcule la distance entre 2 triangles (distance entre les barycentres)
         * 
         * @param t le triangle par rapport auquel on calcule la distance
         * @return la distance entre les 2 triangles
        */
        float distance(const Triangle2d &t);


        /**
         * @brief Interpolation linéaire
         * 
         * @return La valeur interpolée
         */
        float interpolate(float y1, float y2, float y3, float x1, float x2, float x3, float y) const;


        /**
         * @brief remplissage d'un triangle 
         * 
         * @param c la couleur avec laquelle on doit colorier le triangle
         * @return rien
        */
        void remplissage(SDL_Renderer* renderer, const SDL_Color &c) const;


};





class Triangle3d
{
    private:
        // un triangle est composé de 3 points
        Point3d p1_;
        Point3d p2_;
        Point3d p3_;

        //Couleur du triangle
        SDL_Color color_;

    

    public:
        // ---------------------------------------------- Constructeurs & Destructeur ----------------------------------------------
        
        /**
         * @brief Constructeur par défaut et valué
         * 
         * @param p1 premier point constituant le triangle
         * @param p2 deuxième point constitant le triangle
         * @param p3 troisième point constitant le triangle
        */
        Triangle3d(const Point3d &p1 = Point3d(), const Point3d &p2 = Point3d(), const Point3d &p3 = Point3d());
        
        
        /**
         * @brief Constructeur de copie
         * 
         * @param t le triangle à copier
        */
        Triangle3d(const Triangle3d &t);


        /**
         * @brief Constructeur de copie d'un triangle 2d
         * 
         * @param t le triangle à copier
        */
        Triangle3d(const Triangle2d &t);





        // ---------------------------------------------- Getters & Setters ----------------------------------------------

        /**
         * @brief Getter du premier point du triangle
         * 
         * @return Le premier point constituant le triangle
        */
        Point3d getP1() const;


        /**
         * @brief Getter du deuxième point du triangle
         * 
         * @return Le deuxième point constituant le triangle
        */
        Point3d getP2() const;


        /**
         * @brief Getter du troisième point du triangle
         * 
         * @return Le troisième point constituant le triangle
        */
        Point3d getP3() const;


        /**
         * @brief Getter de la couleur du triangle
         * 
         * @return La couleur du triangle
        */
        SDL_Color getColor() const;
        

        /**
         * @brief Setter du premier point du triangle
         * 
         * @param p le point à poser comme premier point du triangle
         * @return rien
        */
        void setP1(Point3d p);
        

        /**
         * @brief Setter du deuxième point du triangle
         * 
         * @param p le point à poser comme deuxième point du triangle
         * @return rien
        */
        void setP2(Point3d p);


        /**
         * @brief Setter du troisième point du triangle
         * 
         * @param p le point à poser comme troisième point du triangle
         * @return rien
        */
        void setP3(Point3d p);


        /**
         * @brief Setter de la couleur du triangle
         * 
         * @param color couleur à donner au triangle
         * @return rien
        */
        void setColor(SDL_Color color);



        // ---------------------------------------------- Surcharge d'opérateurs ----------------------------------------------

        /**
         * @brief Teste l'égalité entre 2 triangles
         * 
         * @param t le triangle à comparer
         * @return true si les 2 triangles sont égaux, false sinon
        */
        bool operator==(const Triangle3d &t) const;


        /**
         * @brief Teste la différence entre 2 triangles
         * 
         * @param t le triangle à comparer
         * @return true si les 2 triangles ne sont pas égaux, false sinon
        */
        bool operator!=(const Triangle3d &t) const;


        /**
         * @brief affecte à un triangle un autre triangle
         * 
         * @param t le triangle à affecter
         * @return le nouveau triangle
        */
        Triangle3d & operator=(const Triangle3d &t);


        // ---------------------------------------------- Autres méthodes ----------------------------------------------
        
        /**
         * @brief Fonction qui créée un triangle rectangle. Elle prend en argument 2 points et place le troisième de façon 
         * à former un triangle rectangle
         * 
         * @param p1 Premier point du triangle
         * @param p2 Deuxième point du triangle
         * @return le triangle créé
        */
        static Triangle3d triangleRectangle(const Point3d &p1, const Point3d &p2); // Juste 2 points car le troisième sera créé pour qu'il soit rectangle
        
        
        /**
         * @brief fonction qui teste si le triangle est rectangle
         *
         * @return true si le triangle est rectangle, false sinon
         */
        bool isRectangle();


        /**
         * @brief décale le triangle
         * 
         * @param dx déplacement selon l'axe x
         * @param dy déplacement selon l'axe y
         * @param dz déplacement selon l'axe z
         * @return rien
        */
        void move(float dx, float dy, float dz);


        /**
         * @brief calcule la distance entre 2 triangles (distance entre les barycentres)
         * 
         * @param t le triangle par rapport auquel on calcule la distance
         * @return la distance entre les 2 triangles
        */
        float distance(const Triangle3d &t);



        
        /**
         * @brief Interpolation linéaire
         * 
         * @return La valeur interpolée
         */
        float interpolate(float y1, float y2, float y3, float x1, float x2, float x3, float z1, float z2, float z3, float y) const;


        /**
         * @brief remplissage d'un triangle 
         * 
         * @param c la couleur avec laquelle on doit colorier le triangle
         * @return rien
        */
        void remplissage(SDL_Renderer* renderer, const SDL_Color &c) const;
};





#endif