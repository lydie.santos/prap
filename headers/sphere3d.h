/** 
* \file sphere3d.h
* This file contains the declaration of the sphere3d class
*/

#ifndef SPHERE3D_H
#define SPHERE3D_H

#include "quad3d.h"
#include <vector>


class Sphere
{
    private:
        std::vector<Quad> quads_; // l'ensemble de quadrilatères formant la sphère


    public:
        // ---------------------------------------------- Constructeurs & Destructeur ----------------------------------------------
        
        /**
         * @brief Constructeur par défaut
         */
        Sphere();

        /**
         * @brief Constructeur valué
         * 
         * @param q ensemble des quadrilatère formant la sphère
        */
        Sphere(const std::vector<Quad> &q);

        /**
         * @brief Constructeur de copie
         * 
         * @param s la sphere à copier
        */
        Sphere(const Sphere & s);





        // ---------------------------------------------- Getters & Setters ----------------------------------------------
        
        /**
         * @brief Getter de quads
         * 
         * @return un pointeur vers l'ensemble de quadrilatère qui compose la sphère
        */
        std::vector<Quad> getQuads() const;

        /**
         * @brief Setter de quads
         * 
         * @param q l'ensemble de quadrilatère à donner à quads
         * @return rien
        */
        void setQuads(std::vector<Quad> q);





        // ---------------------------------------------- Surcharge d'opérateurs ----------------------------------------------
        
        /**
         * @brief Teste l'égalité entre 2 spheres
         * 
         * @param s la sphère à comparer
         * @return true si les 2 sphères sont égales, false sinon
        */
        bool operator==(const Sphere &s) const;



        /**
         * @brief affecte à une sphère une autre sphère
         * 
         * @param s la sphère à affecter
         * @return la nouvelle sphère
        */
        Sphere & operator=(const Sphere &s);




        // ---------------------------------------------- Autres méthodes ----------------------------------------------
        
        /**
         * @brief décale la sphère
         * 
         * @param dx déplacement selon l'axe x
         * @param dy déplacement selon l'axe y
         * @param dz déplacement selon l'axe z
         * @return rien
        */
        void move(float dx, float dy, float dz);

        /**
         * @brief calcule la distance entre 2 sphère
         * 
         * @param s la sphère par rapport à laquelle on calcule la distance
         * @return la distance entre les 2 sphères
        */
        float distance(const Sphere &s);
};

#endif