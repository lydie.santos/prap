/** 
* \file sdl.h
* This file contains the declaration of the sdl class that eased the use of SDL
*/

#ifndef SDL_H
#define SDL_H

#include <SDL2/SDL.h>
#include "pave3d.h"
#include "sphere3d.h"
#include "scene3d.h"
#include "types.h"

#include <list>




class Sdl
{
    private:
        /**
         * booléen qui indiquant si la boucle de la méthode run est en cours
         * Si is_running vaut false, la boucle de la méthode run est arrêtée
        */
        static bool is_running;

        /**
         * Liste d'événements possible
        */
        static std::list<pevent> events; 

        /**
         * Fenêtre
        */
        static void* win_;

        /**
            * Renderer
        */
        static void* ren_;


    public:


        /**
         * @brief récupération de renderer
         * @return le renderer
        */
       static void* getRen();

        /**
         * @brief initialisation de sdl
         * 
         * @return rien
        */
        static void init();


        /**
         * @brief termine la sdl
         * 
         * @return rien
        */
        static void quit();


        /**
         * @brief Initialise la fenêtre
         * 
         * @param titre le titre de la fene+être
         * @param pos la position de la fenêtre
         * @param size la taille de la fenêtre
        */
        static void init_window(std::string &titre, position &pos, size &s);


        /**
         * @brief Destruction de la denêtre
        */
        static void destroy_window();


        /**
         * @brief Affiche la fenêtre
         * 
         * @return rien
        */
        static void show();

        /**
         * @brief Cache la fenêtre
         * 
         * @return rien
        */
        static void hide();


        /**
         * @brief Actualise la fenêtre
         * 
         * @return rien
        */
        static void present();


        /**
         * @brief Créer une fenêtre d'affichage noire
         * 
         * @param name titre de la fenêtre
         * @param width largeur de la fenêtre*
         * @param height hauteur de la fenêtre
         * @return rien
        */
        SDL_Renderer* blackWindow(const char *name, int width, int height);



        /**
         * @brief Fonction qui projete un Point3d sur le plan d'une scène
         * 
         * @param s la scène où l'on projète le point
         * @param p le point à projeter sur la scène
         * @return La projection du point dans la scène
        */
        static Point2d Projection(const Point3d &p);


        /**
         * @brief Fonction qui trace un triangle 2d
         * 
         * @param win fenêtre où l'on trace le triangle
         * @param t triangle à tracer
        */
        static void Draw(const Triangle2d &t);



        /**
         * @brief Fonction qui trace un triangle 3d
         * 
         * @param win fenêtre où l'on trace le triangle
         * @param t triangle à tracer
        */
        static void Draw(const Triangle3d &t);


        /**
         * @brief Fonction qui trace un quadrilatère
         * 
         * @param win fenêtre où l'on trace le quadrilatère
         * @param quad quadrilatère à tracer
        */
        static void Draw(const Quad &quad);


        /**
         * @brief Fonction qui trace un pave
         * 
         * @param win fenêtre où l'on trace le pave
         * @param p pave à tracer
        */
        static void Draw(const Pave3d &p);


        /**
         * @brief Fonction qui trace un sphère
         * 
         * @param win fenêtre où l'on trace le sphère
         * @param s sphère à tracer
        */
        static void Draw(const Sphere &s);
        

        /**
         * @brief Lance la boucle infinie permettant de faire bouger le cube
         * 
         * @return rien
        */
        static void run();


        /**
         * @brief Fonction permettant de sortir de la boucle infini
         * 
         * @return rien
        */
        static void exit_run();


        /**
         * @brief Ajoute un événement à la liste des événements possible
         * 
         * @param e le pointeur vers l'événement à ajouter
         * @return rien
        */
        static void add_event(const pevent & e);
};


#endif